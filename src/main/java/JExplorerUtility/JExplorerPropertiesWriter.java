package JExplorerUtility;

import javafx.application.Platform;
import javafx.stage.Stage;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Properties;

public class JExplorerPropertiesWriter {

    private static Logger LOGGER;
    private Stage stageWithDimensionsToWriteToFile;
    private String directoryToWriteCustomFile;
    private String fileNameToWriteDimensions;
    private Properties jExplorerProperties;

    public JExplorerPropertiesWriter(Logger logger, Stage stageWithDimensionsToWriteToFile){
        LOGGER = logger;
        this.stageWithDimensionsToWriteToFile = stageWithDimensionsToWriteToFile;
        jExplorerProperties = new Properties();
    }

    public void savePropertiesToFileAndCloseApplication(){
        File directory = new File(directoryToWriteCustomFile);
        if(!directory.exists()){
            directory.mkdir();
        }
        try {
            writePropertiesToFile(directory);
        } catch (IOException e) {
            if(LOGGER != null){
                LOGGER.error(this.getClass().getName() + ": " + e.getMessage());
            }
        }
        Platform.exit();
    }

    private void writePropertiesToFile(File directory) throws IOException {
        jExplorerProperties.setProperty("WindowWidth", String.valueOf(stageWithDimensionsToWriteToFile.getWidth()));
        jExplorerProperties.setProperty("WindowHeight", String.valueOf(stageWithDimensionsToWriteToFile.getHeight()));
        FileOutputStream fileOutputStream = new FileOutputStream(directory.getAbsolutePath() + "\\" + fileNameToWriteDimensions);
        jExplorerProperties.store(fileOutputStream, null);
        fileOutputStream.close();
    }

    public String getDirectoryToWriteCustomFile() {
        return directoryToWriteCustomFile;
    }

    public void setDirectoryToWriteCustomFile(String directoryToWriteCustomFile) {
        this.directoryToWriteCustomFile = directoryToWriteCustomFile;
    }

    public String getFileNameToWriteDimensions() {
        return fileNameToWriteDimensions;
    }

    public void setFileNameToWriteDimensions(String fileNameToWriteDimensions) {
        this.fileNameToWriteDimensions = fileNameToWriteDimensions;
    }
}
