package JExplorerUtility;

import jExplorerView.utility.UserInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Properties;

public class JExplorerPropertiesReader {

    /*
     *  This class handles restoring the properties to the previous state it was in when the user closed JExplorer.
     */

    private static final Logger LOGGER = LogManager.getLogger("JExplorer");
    private final double WINDOW_DEFAULT_DIMENSIONS = 500;
    private double windowWidthPref = WINDOW_DEFAULT_DIMENSIONS;
    private double windowHeightPref = WINDOW_DEFAULT_DIMENSIONS;
    private File configFile;

    public JExplorerPropertiesReader(){

        Properties jExplorerPropertiesFile = new Properties();
        setConfigFile(new File("C:\\Users\\" + UserInfo.getInstance().getUserName() +
                "\\JExplorer\\JExplorer.properties"));
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(getConfigFile()));
            jExplorerPropertiesFile.load(bufferedReader);
            setPreviousWindowWidthPref(Double.parseDouble(jExplorerPropertiesFile.getProperty("WindowWidth")));
            setPreviousWindowHeightPref(Double.parseDouble(jExplorerPropertiesFile.getProperty("WindowHeight")));
            bufferedReader.close();
        } catch (IOException e) {
            LOGGER.error(this.getClass().getName() + ": " + e.getMessage());
        }

    }

    public double getPreviousWindowHeightPref() {
        return windowHeightPref;
    }

    public void setPreviousWindowHeightPref(double windowHeightPref) {
        this.windowHeightPref = windowHeightPref;
    }

    public double getPreviousWindowWidthPref() {
        return windowWidthPref;
    }

    public void setPreviousWindowWidthPref(double windowWidthPref) {
        this.windowWidthPref = windowWidthPref;
    }

    public File getConfigFile() {
        return configFile;
    }

    public void setConfigFile(File configFile) {
        this.configFile = configFile;
    }
}
