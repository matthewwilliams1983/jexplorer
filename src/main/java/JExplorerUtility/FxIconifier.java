package JExplorerUtility;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class FxIconifier {
    private static FxIconifier instance;

    private FxIconifier(){

    }

    public static FxIconifier getInstance(){
        if(instance == null){
            instance = new FxIconifier();
        }
        return instance;
    }

    public Node iconifyFileIcon(File file){
        Icon icon = FileSystemView.getFileSystemView().getSystemIcon(file);
        BufferedImage img = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = img.createGraphics();
        icon.paintIcon(null, g2d, 0, 0);
        g2d.dispose();
        Image fxIcon = SwingFXUtils.toFXImage(img, null);
        Node imIcon = new ImageView(fxIcon);
        return imIcon;
    }

    public Node iconifyFromResource(String resourceLocation){
        return new ImageView(new Image(ClassLoader.getSystemResourceAsStream(resourceLocation)));
    }

}
