package jExplorerData;

import jExplorerData.jExplorerDirectoryUtility.TreeItemMaker;
import javafx.scene.control.TreeItem;
import jExplorerData.jExplorerDirectoryUtility.JExplorerTreeItemEntry;
import JExplorerUtility.FxIconifier;
import jExplorerView.utility.UserInfo;

import java.io.File;
import java.util.ArrayList;

public class DesktopDirectory extends ArrayList<TreeItem> {
    public DesktopDirectory() {
        TreeItemMaker treeItemMaker = new TreeItemMaker();
        File desktopFile = new File("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\Desktop");
        File[] desktopFiles = desktopFile.listFiles();
        for (File file : desktopFiles) {
            if (!file.getName().equalsIgnoreCase("desktop.ini") && file.isDirectory()) {
                TreeItem<JExplorerTreeItemEntry> treeItem = new TreeItem<>(new JExplorerTreeItemEntry(file.getAbsolutePath(), file.getName()), FxIconifier.getInstance().iconifyFileIcon(file));
                treeItemMaker.createDirectoryTreeItem(treeItem, file);
                this.add(treeItem);
            }
        }
    }
}
