package jExplorerData.jExplorerDirectoryUtility;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import JExplorerUtility.FxIconifier;

import java.io.File;

public class TreeItemMaker {
    public TreeItemMaker() {

    }

    public void createDirectoryTreeItem(TreeItem<JExplorerTreeItemEntry> treeItem, File file) {
        Platform.runLater(() -> {
            File[] childrenFiles = file.listFiles();//Get the list of every child of the directory tree item
            if (childrenFiles != null) {
                for(File childFile: childrenFiles){
                    if(childFile.isDirectory()){//If one of those files is a directory, add a null child
                        treeItem.getChildren().add(null);
                        addListenerForTreeItemExpanded(treeItem, file);
                        break;//Break because we only want one placeholder item
                    }
                }
            }
        });
    }

    private void addListenerForTreeItemExpanded(TreeItem<JExplorerTreeItemEntry> treeItem, File file) {
        treeItem.expandedProperty().addListener(event -> {
            if (treeItem.getChildren().get(0) == null) {
                treeItem.getChildren().remove(0);
                File[] files = file.listFiles();
                if (files != null) {
                    for (File fileCurrent : files) {
                        if (fileCurrent.isDirectory()) {
                            Node iconNode = FxIconifier.getInstance().iconifyFileIcon(fileCurrent);
                            TreeItem<JExplorerTreeItemEntry> treeItem1 = new TreeItem<>(new JExplorerTreeItemEntry(fileCurrent.getAbsolutePath(), fileCurrent.getName()), iconNode);
                            treeItem.getChildren().add(treeItem1);
                            createDirectoryTreeItem(treeItem1, fileCurrent);
                        }
                    }
                }
            }
        });
    }
}
