package jExplorerData.jExplorerDirectoryUtility;

import javafx.scene.Node;

public class JExplorerTreeItemEntry {
    private String absolutePath;
    private String fileName;
    private Node iconNode;

    public JExplorerTreeItemEntry(String fullPath, String fileName){
        this.absolutePath = fullPath;
        this.fileName = fileName;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return this.fileName;
    }

    public Node getIconNode() {
        return iconNode;
    }

    public void setIconNode(Node iconNode) {
        this.iconNode = iconNode;
    }
}
