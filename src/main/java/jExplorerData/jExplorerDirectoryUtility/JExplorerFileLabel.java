package jExplorerData.jExplorerDirectoryUtility;

import javafx.scene.control.Label;
import JExplorerUtility.FxIconifier;

import java.io.File;

public class JExplorerFileLabel extends Label {
    public JExplorerFileLabel(File fileToLabel){
        this.getStylesheets().add("css/defaultTheme.css");
        this.setId("jExplorerFileLabel");
        this.setGraphic(FxIconifier.getInstance().iconifyFileIcon(fileToLabel));
        this.setText(fileToLabel.getName());
    }
    public JExplorerFileLabel(File fileToLabel, String fileName){
        this.getStylesheets().add("css/defaultTheme.css");
        this.setId("jExplorerFileLabel");
        this.setGraphic(FxIconifier.getInstance().iconifyFileIcon(fileToLabel));
        this.setText(fileName);
    }
}
