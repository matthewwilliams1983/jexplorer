package jExplorerData.jExplorerDirectoryUtility;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.Stack;

public class JExplorerNavigationHistory {

    private PropertyChangeSupport propertyChangeSupport;
    private Stack<File> backStack;
    private Stack<File> forwardStack;
    private File poppedFile;

    public JExplorerNavigationHistory(){
        propertyChangeSupport = new PropertyChangeSupport(this);
        backStack = new Stack<>();
        forwardStack = new Stack<>();
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public boolean isBackStackEmpty(){
        return this.getBackStack().isEmpty();
    }

    public boolean isForwardStackEmpty(){
        return this.getForwardStack().isEmpty();
    }

    public String getPoppedFileAbsolutePath(){
        return getPoppedFile().getAbsolutePath();
    }

    public Stack<File> getBackStack() {
        return backStack;
    }

    private void setBackStack(Stack<File> backStack) {
        this.backStack = backStack;
    }

    public Stack<File> getForwardStack() {
        return forwardStack;
    }

    private void setForwardStack(Stack<File> forwardStack) {
        this.forwardStack = forwardStack;
    }

    public File getPoppedFile() {
        return poppedFile;
    }

    public void setPoppedFile(File poppedFile) {
        File previousPoppedFile = this.poppedFile;
        this.poppedFile = poppedFile;
        propertyChangeSupport.firePropertyChange("setPoppedFile", previousPoppedFile, poppedFile);
    }

    @Override
    public String toString() {

        String forwardStackValues;
        String backwardStackValues;

        if(!this.forwardStack.isEmpty()){
            forwardStackValues = "Forward Stack: " + this.forwardStack.toString();
        }else{
            forwardStackValues = "Forward Stack: ";
        }
        if(!this.backStack.isEmpty()){
            backwardStackValues = "\n" + "Back Stack: " + this.backStack.toString();
        }else{
            backwardStackValues = "\n" + "Back Stack: ";
        }

        return forwardStackValues + backwardStackValues;
    }
}
