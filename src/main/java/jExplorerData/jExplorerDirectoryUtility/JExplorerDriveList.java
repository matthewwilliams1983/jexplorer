package jExplorerData.jExplorerDirectoryUtility;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.util.ArrayList;

public class JExplorerDriveList {

    private ArrayList<JExplorerDrive> driveList;

    public JExplorerDriveList(){

        this.setDriveList(new ArrayList<>());

        File[] drives = File.listRoots();
        FileSystemView fileSystemView = FileSystemView.getFileSystemView();

        for(File drive: drives){
            if(fileSystemView.getSystemTypeDescription(drive).equals("CD Drive")){//TODO implement watch to to get name
                this.getDriveList().add(new JExplorerDrive(drive.getAbsolutePath(), fileSystemView.getSystemTypeDescription(drive)));
            }else{
                this.getDriveList().add(new JExplorerDrive(drive.getAbsolutePath(), fileSystemView.getSystemDisplayName(drive)));
            }
        }
    }

    public ArrayList<JExplorerDrive> getDriveList() {
        return driveList;
    }

    public void setDriveList(ArrayList<JExplorerDrive> driveList) {
        this.driveList = driveList;
    }

    public class JExplorerDrive extends File{
        private String driveName;
        public JExplorerDrive(String pathName, String driveName){
           super(pathName);
           this.setDriveName(driveName);
        }

        public String getDriveName() {
            return driveName;
        }

        public void setDriveName(String driveName) {
            this.driveName = driveName;
        }
    }
}