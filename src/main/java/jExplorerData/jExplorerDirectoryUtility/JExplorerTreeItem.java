package jExplorerData.jExplorerDirectoryUtility;

import JExplorerUtility.FxIconifier;
import jExplorerData.jExplorerDirectoryTreeItemsInterfaces.JExplorerTreeable;
import javafx.scene.control.TreeItem;

import java.io.File;

public class JExplorerTreeItem extends TreeItem<JExplorerTreeItemEntry> implements JExplorerTreeable {

    File directory;

    public JExplorerTreeItem(String pathName){
        boolean fileDidExist = setFilePathToDirectory(pathName);
        if(fileDidExist){
            createJExplorerTreeItemEntry();
            makeCustomTreeItem();
        }
    }

    public JExplorerTreeItem(String pathName, String imageResourcePath){
        boolean fileDidExist = setFilePathToDirectory(pathName);
        if(fileDidExist){
            createJExplorerTreeItemEntry(imageResourcePath);
            makeCustomTreeItem();
        }
    }

    @Override
    public boolean setFilePathToDirectory(String pathName) {
        directory = new File(pathName);
        if(directory.exists()){
            return true;
        }
        return false;
    }

    @Override
    public void createJExplorerTreeItemEntry() {
        this.setValue(new JExplorerTreeItemEntry(directory.getAbsolutePath(), directory.getName()));
        this.setGraphic(FxIconifier.getInstance().iconifyFileIcon(directory));
    }

    public void createJExplorerTreeItemEntry(String imageResourcePath) {
        this.setValue(new JExplorerTreeItemEntry(directory.getAbsolutePath(), directory.getName()));
        this.setGraphic(FxIconifier.getInstance().iconifyFromResource(imageResourcePath));
    }

    @Override
    public void makeCustomTreeItem() {
        TreeItemMaker treeItemMaker = new TreeItemMaker();
        treeItemMaker.createDirectoryTreeItem(this, directory);
    }
}