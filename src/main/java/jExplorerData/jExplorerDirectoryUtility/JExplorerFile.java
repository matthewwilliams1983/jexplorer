package jExplorerData.jExplorerDirectoryUtility;

import javafx.scene.control.Label;

import java.io.File;
import java.text.SimpleDateFormat;

public class JExplorerFile extends File {

    private Label fileIconWithName;
    private String fileSize;
    private String lastModified;
    private String fileType;

    public JExplorerFile(String pathName) {
        super(pathName);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        this.lastModified = simpleDateFormat.format(this.lastModified());
        this.fileIconWithName = new JExplorerFileLabel(this);

        if (!this.isDirectory()) {
            generateFileValues(pathName);
        }else{
            generateFolderValues();
        }
    }
    public JExplorerFile(String pathName, String fileName) {
        super(pathName);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        this.lastModified = simpleDateFormat.format(this.lastModified());
        this.fileIconWithName = new JExplorerFileLabel(this, fileName);

        if (!this.isDirectory()) {
            generateFileValues(pathName);
        }else{
            generateFolderValues();
        }
    }

    private void generateFileValues(String pathName) {
        setFileExt(pathName);
        generateInitFileSize();
    }

    private void setFileExt(String pathName) {
        int indexOfDot = pathName.lastIndexOf(".");
        this.fileType = (indexOfDot == -1) ? "" : pathName.substring(indexOfDot + 1).toUpperCase();
    }

    private void generateInitFileSize() {
        long tempFileSize = this.length() / 1024;
        if(tempFileSize == 0){
            this.fileSize = String.valueOf(1 + "  KB");
        }else{
            this.fileSize = String.valueOf(this.length() / 1024) + " KB";
        }
    }

    public String getFileExt(String pathName) {
        int indexOfDot = pathName.lastIndexOf(".");
        return (indexOfDot == -1) ? "" : pathName.substring(indexOfDot + 1).toUpperCase();
    }

    private void generateFolderValues() {
        this.fileSize = "";
        this.fileType = "File folder";
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Label getFileIconWithName() {
        return fileIconWithName;
    }

    public void setFileIconWithName(Label fileIconWithName) {
        this.fileIconWithName = fileIconWithName;
    }
}
