package jExplorerData;

import jExplorerData.jExplorerDirectoryTreeItemsInterfaces.JExplorerTreeable;
import jExplorerData.jExplorerDirectoryUtility.TreeItemMaker;
import JExplorerUtility.FxIconifier;
import javafx.scene.control.TreeItem;
import jExplorerData.jExplorerDirectoryUtility.JExplorerTreeItemEntry;
import jExplorerView.utility.UserInfo;

import java.io.File;

public class JExplorerUserDirectoryTreeItem extends TreeItem<JExplorerTreeItemEntry> implements JExplorerTreeable {

    private File userDesktopDirectory;

    public JExplorerUserDirectoryTreeItem(String path){
        boolean fileDidExist = setFilePathToDirectory(path);
        if(fileDidExist){
            createJExplorerTreeItemEntry();
            makeCustomTreeItem();
        }
    }

    @Override
    public boolean setFilePathToDirectory(String path) {
        userDesktopDirectory = new File(path);
        return userDesktopDirectory.exists();
    }

    @Override
    public void createJExplorerTreeItemEntry() {
        super.setValue(new JExplorerTreeItemEntry(userDesktopDirectory.getAbsolutePath(), UserInfo.getInstance().getUserName()));
        this.setGraphic(FxIconifier.getInstance().iconifyFromResource("images/UserIcon.png"));
    }

    @Override
    public void makeCustomTreeItem() {
        TreeItemMaker treeItemMaker = new TreeItemMaker();
        treeItemMaker.createDirectoryTreeItem(this, userDesktopDirectory);
    }
}
