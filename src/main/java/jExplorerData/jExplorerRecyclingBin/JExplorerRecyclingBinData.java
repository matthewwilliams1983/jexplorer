package jExplorerData.jExplorerRecyclingBin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class JExplorerRecyclingBinData implements Runnable {

    private Map<String, String> recyclingBinData;

    public JExplorerRecyclingBinData(){
        setRecyclingBinData(new HashMap<>());
    }

    public Map<String, String> getRecyclingBinData() {
        return recyclingBinData;
    }

    public void setRecyclingBinData(Map<String, String> recyclingBinData) {
        this.recyclingBinData = recyclingBinData;
    }

    @Override
    public void run() {
        String getRecyclePath = "powershell.exe (New-Object -ComObject Shell.Application).NameSpace(0x0a).Items()|select Path -Expand Path";
        String getRecycleFileName = "powershell.exe (New-Object -ComObject Shell.Application).NameSpace(0x0a).Items()|select Name -Expand Name";
        try {
            Process powerShellProcessPath = Runtime.getRuntime().exec(getRecyclePath);
            Process powerShellProcessFileName = Runtime.getRuntime().exec(getRecycleFileName);
            powerShellProcessPath.getOutputStream().close();
            powerShellProcessFileName.getOutputStream().close();
            String recyclingBinPath;
            String recyclingBinFileName;
            BufferedReader filePathReader = new BufferedReader(new InputStreamReader(powerShellProcessPath.getInputStream()));
            BufferedReader fileNameReader = new BufferedReader(new InputStreamReader(powerShellProcessFileName.getInputStream()));
            while((recyclingBinPath = filePathReader.readLine()) != null && (recyclingBinFileName = fileNameReader.readLine()) != null){
                getRecyclingBinData().put(recyclingBinPath, recyclingBinFileName);
            }
            filePathReader.close();
            fileNameReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
