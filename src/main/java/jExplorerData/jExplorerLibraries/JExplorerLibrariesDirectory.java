package jExplorerData.jExplorerLibraries;

import JExplorerUtility.FxIconifier;
import jExplorerData.jExplorerDirectoryUtility.JExplorerTreeItem;
import javafx.scene.control.TreeItem;
import jExplorerData.jExplorerDirectoryUtility.JExplorerTreeItemEntry;
import jExplorerView.utility.UserInfo;

import java.io.File;

public class JExplorerLibrariesDirectory extends TreeItem<JExplorerTreeItemEntry>{

    public JExplorerLibrariesDirectory(){
        File fileLib = new File("C:\\Users\\"+ UserInfo.getInstance().getUserName() +"\\AppData\\Roaming\\Microsoft\\Windows\\Libraries");
        this.setValue(new JExplorerTreeItemEntry(fileLib.getAbsolutePath(), fileLib.getName()));
        this.setGraphic(FxIconifier.getInstance().iconifyFromResource("images/LibrariesIcon.png"));
        this.getChildren().add(new JExplorerTreeItem("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\Documents"));
        this.getChildren().add(new JExplorerTreeItem("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\Music"));
        this.getChildren().add(new JExplorerTreeItem("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\Pictures"));
        this.getChildren().add(new JExplorerTreeItem("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\Videos"));
    }
}
