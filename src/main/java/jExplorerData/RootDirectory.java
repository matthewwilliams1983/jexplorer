package jExplorerData;

import jExplorerControllers.JExplorerStartViewController;
import jExplorerData.jExplorerLibraries.JExplorerLibrariesDirectory;
import jExplorerData.jExplorerThisPC.JExplorerThisPCDirectoryTreeItem;
import jExplorerData.jExplorerDirectoryUtility.JExplorerTreeItem;
import javafx.scene.control.TreeItem;
import jExplorerData.jExplorerDirectoryUtility.JExplorerTreeItemEntry;
import JExplorerUtility.FxIconifier;
import jExplorerView.utility.UserInfo;

import java.io.File;

public class RootDirectory extends TreeItem<JExplorerTreeItemEntry> {
    public RootDirectory(){

        File rootDirectory = new File("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\Desktop");

        this.setValue(new JExplorerTreeItemEntry(rootDirectory.getAbsolutePath(), "Desktop"));
        this.setGraphic(FxIconifier.getInstance().iconifyFileIcon(rootDirectory));
        this.setExpanded(true);

        JExplorerTreeItem userDirectory = new JExplorerTreeItem("C:\\Users\\" + UserInfo.getInstance().getUserName(), "images/UserIcon.png");
        JExplorerLibrariesDirectory librariesDirectory = new JExplorerLibrariesDirectory();
        DesktopDirectory desktopDirectory = new DesktopDirectory();
        JExplorerThisPCDirectoryTreeItem jExplorerThisPCDirectory = new JExplorerThisPCDirectoryTreeItem();
        JExplorerTreeItem jExplorerRecycleBinTreeItem = new JExplorerTreeItem("C:\\$Recycle.Bin\\" + UserInfo.getInstance().getUserSID());

        this.getChildren().add(userDirectory);
        this.getChildren().add(librariesDirectory);
        this.getChildren().add(jExplorerThisPCDirectory);
        this.getChildren().add(jExplorerRecycleBinTreeItem);
        for(TreeItem desktopItem:desktopDirectory){
            this.getChildren().add(desktopItem);
        }

        JExplorerStartViewController.getInstance().setTreeItemEntryToSelectOnLoad(jExplorerThisPCDirectory);

    }

}
