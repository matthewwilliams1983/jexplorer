package jExplorerData.jExplorerDirectoryTreeItemsInterfaces;

public interface JExplorerTreeable {
    boolean setFilePathToDirectory(String path);
    void createJExplorerTreeItemEntry();
    void makeCustomTreeItem();
}
