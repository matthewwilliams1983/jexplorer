package jExplorerData.jExplorerThisPC;

import jExplorerData.jExplorerDirectoryTreeItemsInterfaces.JExplorerTreeable;
import jExplorerData.jExplorerDirectoryUtility.JExplorerDriveList;
import jExplorerData.jExplorerDirectoryUtility.JExplorerTreeItemEntry;
import jExplorerData.jExplorerDirectoryUtility.TreeItemMaker;
import JExplorerUtility.FxIconifier;
import javafx.scene.control.TreeItem;


public class JExplorerDriveDirectoryTreeItem extends TreeItem<JExplorerTreeItemEntry> implements JExplorerTreeable {

    private JExplorerDriveList.JExplorerDrive drive;

    public JExplorerDriveDirectoryTreeItem(JExplorerDriveList.JExplorerDrive drive){
        this.drive = drive;
        createJExplorerTreeItemEntry();
        makeCustomTreeItem();
    }

    public boolean setFilePathToDirectory(String path) {
        //No Op - file path defined in JExplorerDriveList
        return true;
    }

    @Override
    public void createJExplorerTreeItemEntry() {
        setValue(new JExplorerTreeItemEntry(drive.getAbsolutePath(), drive.getDriveName()));
        setGraphic(FxIconifier.getInstance().iconifyFileIcon(drive));
    }

    @Override
    public void makeCustomTreeItem() {
        TreeItemMaker treeItemMaker = new TreeItemMaker();
        treeItemMaker.createDirectoryTreeItem(this, drive);
    }
}
