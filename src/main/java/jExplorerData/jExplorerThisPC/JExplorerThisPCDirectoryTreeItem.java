package jExplorerData.jExplorerThisPC;

import jExplorerData.jExplorerDirectoryUtility.JExplorerTreeItem;
import jExplorerData.jExplorerDirectoryUtility.JExplorerTreeItemEntry;
import JExplorerUtility.FxIconifier;
import javafx.scene.control.TreeItem;

import java.io.File;

public class JExplorerThisPCDirectoryTreeItem extends TreeItem<JExplorerTreeItemEntry> {

    public JExplorerThisPCDirectoryTreeItem(){

        this.setValue(new JExplorerTreeItemEntry("This PC", "This PC"));
        this.setGraphic(FxIconifier.getInstance().iconifyFromResource("images/ThisPCIcon.PNG"));

        JExplorerThisPCDirectory jExplorerThisPCDirectory = new JExplorerThisPCDirectory();
        for(File directory: jExplorerThisPCDirectory.getDirectoryList()){
            this.getChildren().add(new JExplorerTreeItem(directory.getAbsolutePath()));
        }

    }
}
