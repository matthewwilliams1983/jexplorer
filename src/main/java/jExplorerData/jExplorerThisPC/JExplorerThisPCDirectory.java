package jExplorerData.jExplorerThisPC;

import jExplorerData.jExplorerDirectoryUtility.JExplorerDriveList;
import jExplorerView.utility.UserInfo;

import java.io.File;
import java.util.ArrayList;

public class JExplorerThisPCDirectory {

    private ArrayList<File> directoryList;

    public JExplorerThisPCDirectory(){//TODO check THISPC directory from registry

        directoryList = new ArrayList<>();

        File threeDObjectDirectory = new File("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\3D Objects");
        addFileToDirectoryListIfExist(threeDObjectDirectory);

        File desktopDirectory = new File("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\Desktop");
        addFileToDirectoryListIfExist(desktopDirectory);

        File documentsDirectory = new File("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\Documents");
        addFileToDirectoryListIfExist(documentsDirectory);

        File downloadsDirectory = new File("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\Downloads");
        addFileToDirectoryListIfExist(downloadsDirectory);

        File musicDirectory = new File("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\Music");
        addFileToDirectoryListIfExist(musicDirectory);

        File picturesDirectory = new File("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\Pictures");
        addFileToDirectoryListIfExist(picturesDirectory);

        File videosDirectory = new File("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\Videos");
        addFileToDirectoryListIfExist(videosDirectory);

        JExplorerDriveList jExplorerDriveList = new JExplorerDriveList();
        for(JExplorerDriveList.JExplorerDrive drive: jExplorerDriveList.getDriveList()){
            addDriveToDirectoryList(drive);
        }
    }

    private void addFileToDirectoryListIfExist(File file){
        if(file.exists()){
            this.getDirectoryList().add(file);
        }
    }

    private void addDriveToDirectoryList(File drive){
        this.getDirectoryList().add(drive);
    }

    public void setDirectoryList(ArrayList<File> directoryList) {
        this.directoryList = directoryList;
    }

    public ArrayList<File> getDirectoryList() {
        return directoryList;
    }
}
