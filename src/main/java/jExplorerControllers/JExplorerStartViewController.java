package jExplorerControllers;

import jExplorerData.jExplorerDirectoryUtility.JExplorerTreeItemEntry;
import jExplorerView.jExplorerContainers.jExplorerCenterBorderPane.JExplorerCenterFileExplorerTableView;
import jExplorerView.jExplorerContainers.jExplorerLeftBorderPane.JExplorerLeftSideExplorerTreeView;
import javafx.scene.control.TreeItem;

public class JExplorerStartViewController {

    private static JExplorerStartViewController jExplorerStartViewController;

    private JExplorerLeftSideExplorerTreeView jExplorerLeftSideExplorerTreeView;
    private TreeItem<JExplorerTreeItemEntry> treeItemEntryToSelectOnLoad;
    private JExplorerCenterFileExplorerTableView jExplorerCenterFileExplorerTableView;

    private JExplorerStartViewController(){

    }

    public static JExplorerStartViewController getInstance(){
        if(jExplorerStartViewController == null){
            jExplorerStartViewController = new JExplorerStartViewController();
        }
        return jExplorerStartViewController;
    }

    public void selectTreeItemOnLoad(){
        this.getjExplorerLeftSideExplorerTreeView().getSelectionModel().select(this.getTreeItemEntryToSelectOnLoad());
    }

    public JExplorerLeftSideExplorerTreeView getjExplorerLeftSideExplorerTreeView() {
        return jExplorerLeftSideExplorerTreeView;
    }

    public void setjExplorerLeftSideExplorerTreeView(JExplorerLeftSideExplorerTreeView jExplorerLeftSideExplorerTreeView) {
        this.jExplorerLeftSideExplorerTreeView = jExplorerLeftSideExplorerTreeView;
    }

    public TreeItem<JExplorerTreeItemEntry> getTreeItemEntryToSelectOnLoad() {
        return treeItemEntryToSelectOnLoad;
    }

    public void setTreeItemEntryToSelectOnLoad(TreeItem<JExplorerTreeItemEntry> treeItemEntryToSelectOnLoad) {
        this.treeItemEntryToSelectOnLoad = treeItemEntryToSelectOnLoad;
    }

    public JExplorerCenterFileExplorerTableView getjExplorerCenterFileExplorerTableView() {
        return jExplorerCenterFileExplorerTableView;
    }

    public void setjExplorerCenterFileExplorerTableView(JExplorerCenterFileExplorerTableView jExplorerCenterFileExplorerTableView) {
        this.jExplorerCenterFileExplorerTableView = jExplorerCenterFileExplorerTableView;
    }
}
