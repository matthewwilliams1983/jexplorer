package jExplorerControllers.jExplorerNavigationControllers;

import jExplorerData.jExplorerDirectoryUtility.JExplorerFile;
import jExplorerData.jExplorerRecyclingBin.JExplorerRecyclingBinData;
import jExplorerData.jExplorerThisPC.JExplorerThisPCDirectory;
import jExplorerView.jExplorerContainers.jExplorerCenterBorderPane.JExplorerCenterFileExplorerTableView;
import jExplorerView.jExplorerContainers.jExplorerCenterBorderPane.columns.CenterColumns;
import jExplorerView.jExplorerContainers.jExplorerLeftBorderPane.JExplorerLeftSideExplorerTreeView;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationBar.JExplorerNavigationBar;
import jExplorerView.utility.UserInfo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Cursor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Map;

//TODO.. Update customSideExplorerTreeView when this changes view.
public class JExplorerCenterFileExplorerController {

    private static JExplorerCenterFileExplorerController jExplorerCenterFileExplorerController;
    private static final Logger LOGGER = LogManager.getLogger("JExplorer");

    private ObservableList<JExplorerFile> data;

    private JExplorerCenterFileExplorerTableView jExplorerCenterFileExplorerTableView;
    private JExplorerLeftSideExplorerTreeView jExplorerLeftSideExplorerTreeView;
    private JExplorerRecyclingBinData jExplorerRecyclingBinData;
    private JExplorerNavigationBar jExplorerNavigationBar;
    private JExplorerThisPCDirectory jExplorerThisPCDirectory;

    private JExplorerCenterFileExplorerController(){
        this.data = FXCollections.observableArrayList();
        this.jExplorerThisPCDirectory = new JExplorerThisPCDirectory();
    }

    public static JExplorerCenterFileExplorerController getInstance(){
        if(jExplorerCenterFileExplorerController == null){
            jExplorerCenterFileExplorerController = new JExplorerCenterFileExplorerController();
        }
        return jExplorerCenterFileExplorerController;
    }

    private void setInitView(){
        populateDataWithThisPCDirectory();
        this.setMouseEvents();
        this.setItemsAndColumns();
    }

    private void populateDataWithThisPCDirectory() {
        for (File directory : jExplorerThisPCDirectory.getDirectoryList()) {
            data.add(new JExplorerFile(directory.getAbsolutePath()));
        }
    }

    public void setDataControlCenterFileExplorer(){
        /*
         * Populate this view with the children files and directories from the whatever was selected in the sideView
         * Listen for selection change events in the sideExplorer.
         */
        jExplorerLeftSideExplorerTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            this.setNavigationText(newValue.getValue().getAbsolutePath());

            data.clear();

            if(newValue.getValue().getAbsolutePath().equals("C:\\$Recycle.Bin\\" + UserInfo.getInstance().getUserSID())){

                JExplorerNavigationController.getInstance().addFileToBackHistory(new File(newValue.getValue().getAbsolutePath()));
                this.jExplorerRecyclingBinData = new JExplorerRecyclingBinData();
                jExplorerRecyclingBinData.run();

                for(Map.Entry<String, String> entry: jExplorerRecyclingBinData.getRecyclingBinData().entrySet()){
                    data.add(new JExplorerFile(entry.getKey(), entry.getValue()));
                }

            }else if(newValue.getValue().getAbsolutePath().equals("This PC")){
                populateDataWithThisPCDirectory();
            }else{
                JExplorerNavigationController.getInstance().addFileToBackHistory(new File(newValue.getValue().getAbsolutePath()));

                File[] files = new File(newValue.getValue().getAbsolutePath()).listFiles();
                if (files != null) {
                    for(File file: files){
                        if(!file.getName().equals("desktop.ini"))
                            data.add(new JExplorerFile(file.getAbsolutePath()));
                    }
                }
            }
            this.setMouseEvents();
            this.setItemsAndColumns();
        });

        //Ensure cursor is default
        jExplorerCenterFileExplorerTableView.setOnMouseMoved(event -> {
            jExplorerCenterFileExplorerTableView.setCursor(Cursor.DEFAULT);
        });
    }

    public void openFileOrExpandDirectory(JExplorerFile fileDirectoryToOpenOrExpand) {
        if(fileDirectoryToOpenOrExpand != null){
            if(!fileDirectoryToOpenOrExpand.isDirectory()){
                this.openFile(fileDirectoryToOpenOrExpand);
            }else{
                JExplorerNavigationController.getInstance().addFileToBackHistory(fileDirectoryToOpenOrExpand);
                this.expandDirectory(fileDirectoryToOpenOrExpand);
            }
        }else{
            jExplorerCenterFileExplorerTableView.refresh();
        }
    }//EoM openFileOrExpandDirectory------------------------------------------------------------------------------------

    private void setMouseEvents() {
        //User has double clicked a file or directory
        jExplorerCenterFileExplorerTableView.setOnMouseClicked(event -> {
            if(event.getClickCount() == 2){
                JExplorerFile fileDirectoryDoubleClicked = (JExplorerFile) jExplorerCenterFileExplorerTableView.getSelectionModel().getSelectedItem();
                openFileOrExpandDirectory(fileDirectoryDoubleClicked);
            }
        });
    }

    private void setItemsAndColumns() {
        jExplorerCenterFileExplorerTableView.getColumns().setAll(new CenterColumns());
        jExplorerCenterFileExplorerTableView.setItems(data);//Set this view
    }

    /*
     * Purpose - This method checks if a double click event was on a file or a directory and then calls a helper
     *           method that carries out the correct functionality.
     * @param fileDoubleClicked - The file that was clicked on by the user to be opened.
     */
    private void openFile(JExplorerFile fileDoubleClicked) {
        try {
            Desktop.getDesktop().open(fileDoubleClicked);
        } catch (IOException e) {
            LOGGER.error(this.getClass() + ": " + e.toString());
        }
    }//EoM openFile-----------------------------------------------------------------------------------------------------

    /*
     * Purpose - This method populates the view with subdirectory of the clicked on directory.
     * @param directoryDoubleClicked - The directory that was clicked on by the user to be expanded.
     */
    private void expandDirectory(JExplorerFile directoryDoubleClicked) {//TODO need to create a new method to handle navigation address request
        this.setNavigationText(directoryDoubleClicked.getAbsolutePath());
        File[] files = new File(directoryDoubleClicked.getAbsolutePath()).listFiles();
        data.clear();
        if (files != null) {
            for(File file: files){
                data.add(new JExplorerFile(file.getAbsolutePath()));
            }
        }
        this.setItemsAndColumns();
    }//EoM expandDirectory----------------------------------------------------------------------------------------------

    private void setNavigationText(String absolutePath){
        this.getjExplorerNavigationBar().setValue(null);
        this.getjExplorerNavigationBar().setPromptText(absolutePath);
    }

    //Getters and Setters
    public ObservableList<JExplorerFile> getData() {
        return data;
    }

    public JExplorerCenterFileExplorerTableView getjExplorerCenterFileExplorerTableView() {
        return jExplorerCenterFileExplorerTableView;
    }

    public void setjExplorerCenterFileExplorerTableView(JExplorerCenterFileExplorerTableView jExplorerCenterFileExplorerTableView) {
        this.jExplorerCenterFileExplorerTableView = jExplorerCenterFileExplorerTableView;
    }

    public JExplorerLeftSideExplorerTreeView getjExplorerLeftSideExplorerTreeView() {
        return jExplorerLeftSideExplorerTreeView;
    }

    public void setjExplorerLeftSideExplorerTreeView(JExplorerLeftSideExplorerTreeView jExplorerLeftSideExplorerTreeView) {
        this.jExplorerLeftSideExplorerTreeView = jExplorerLeftSideExplorerTreeView;
    }

    public void setStartView() {
        this.setInitView();
        this.setDataControlCenterFileExplorer();
    }

    public JExplorerNavigationBar getjExplorerNavigationBar() {
        return jExplorerNavigationBar;
    }

    public void setjExplorerNavigationBar(JExplorerNavigationBar jExplorerNavigationBar) {
        this.jExplorerNavigationBar = jExplorerNavigationBar;
    }
}
