package jExplorerControllers.jExplorerNavigationControllers;

//Local imports
import jExplorerData.jExplorerDirectoryUtility.JExplorerFile;
import jExplorerData.jExplorerDirectoryUtility.JExplorerNavigationHistory;
import jExplorerView.jExplorerContainers.jExplorerCenterBorderPane.JExplorerCenterFileExplorerTableView;
import jExplorerView.jExplorerContainers.jExplorerCenterBorderPane.columns.CenterColumns;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationBar.JExplorerNavigationBar;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationButtons.JExplorerNavigationBackButton;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationButtons.JExplorerNavigationForwardButton;

//Standard lib imports
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

//Fx imports
import javafx.scene.input.KeyCode;


/*
 * Purpose - This class controls the navigation history of JExplorer.
 *
 * @author Matthew Williams
 * @since 2018-27-11
 */

public class JExplorerNavigationController {

    //Static controller
    private static JExplorerNavigationController jExplorerNavigationController;

    //View
    private JExplorerNavigationBackButton jExplorerNavigationBackButton;
    private JExplorerNavigationForwardButton jExplorerNavigationForwardButton;
    private JExplorerCenterFileExplorerTableView jExplorerCenterFileExplorerTableView;
    private JExplorerNavigationBar jExplorerNavigationBar;

    //Data
    private JExplorerNavigationHistory jExplorerNavigationHistory;

    //Flags to determine the direction of navigation
    private boolean isFirstPopFrontSide;
    private boolean isFirstPopBackSide;



    /*
     * Purpose - This no args constructor creates a new JExplorerNavigationHistory object as well as sets the
     *           variable flags for the stack state to true.  This is marked as private to ensure that only one
     *           object of JExplorerNavigationController exists inside the program
     *
     */
    private JExplorerNavigationController(){
        setJExplorerNavigationHistory(new JExplorerNavigationHistory());
        setFirstPopFrontSide(true);
        setFirstPopBackSide(true);
    }//EoM JExplorerNavigationController Constructor--------------------------------------------------------------------

    /*
     * Purpose - This method returns the singleton JExplorerNavigationController.
     *
     * @return JExplorerNavigationController - The singleton of a JExplorerNavigationController.
     */
    public static JExplorerNavigationController getInstance(){
        if(jExplorerNavigationController == null){
            jExplorerNavigationController = new JExplorerNavigationController();
        }
        return jExplorerNavigationController;
    }//EoM getInstance--------------------------------------------------------------------------------------------------

    /*
     * Purpose - This method adds a file from the backStack and places it on the forwardStack.
     *
     * @return boolean true/false - The state of if the backStack file could be pushed onto the forwardStack.
     */
    public boolean addCurrentLocationToForwardHistory(){
        setFirstPopFrontSide(true);
        if(!getJExplorerNavigationHistory().isBackStackEmpty()){
            if(!isFirstPopBackSide()){
                adjustHistoryFrowardHelper();
                return true;
            }else{
                getJExplorerNavigationHistory().getForwardStack().push(getJExplorerNavigationHistory().getBackStack().pop());
                adjustHistoryFrowardHelper();
                setFirstPopBackSide(false);
                return true;
            }
        }
        return false;
    }//EoM addCurrentLocationToForwardHistory---------------------------------------------------------------------------

    /*
     * Purpose - This method adds a file from the forwardStack and places it on the backStack.
     *
     * @return boolean true/false - The state of if the forwardStack file could be pushed onto the backStack.
     */
    public boolean addCurrentLocationToBackHistory(){
        setFirstPopBackSide(true);
        if(!getJExplorerNavigationHistory().isForwardStackEmpty()){
            if(!isFirstPopFrontSide()){
                adjustHistoryBackHelper();
                return true;
            }else{
                getJExplorerNavigationHistory().getBackStack().push(getJExplorerNavigationHistory().getForwardStack().pop());
                adjustHistoryBackHelper();
                setFirstPopFrontSide(false);
                return true;
            }
        }
        return false;
    }//EoM addCurrentLocationToBackHistory------------------------------------------------------------------------------

    private void adjustHistoryFrowardHelper(){
        getJExplorerNavigationHistory().setPoppedFile(getJExplorerNavigationHistory().getBackStack().pop());
        getjExplorerNavigationBar().setPromptText(getJExplorerNavigationHistory().getPoppedFileAbsolutePath());
        getJExplorerNavigationHistory().getForwardStack().push(getJExplorerNavigationHistory().getPoppedFile());
    }//EoM adjustHistoryFrowardHelper-----------------------------------------------------------------------------------

    private void adjustHistoryBackHelper(){
        getJExplorerNavigationHistory().setPoppedFile(getJExplorerNavigationHistory().getForwardStack().pop());
        getjExplorerNavigationBar().setPromptText(getJExplorerNavigationHistory().getPoppedFile().getAbsolutePath());
        getJExplorerNavigationHistory().getBackStack().push(getJExplorerNavigationHistory().getPoppedFile());
    }//EoM adjustHistoryBackHelper--------------------------------------------------------------------------------------

    public void addFileToBackHistory(File fileToAdd){
        this.getJExplorerNavigationHistory().getBackStack().push(fileToAdd);
    }//EoM addFileToBackHistory-----------------------------------------------------------------------------------------

    private void setJExplorerNavigationForwardButtonOnClickEvents() {
        this.getJExplorerNavigationForwardButton().setOnMouseClicked(event -> {
            this.addCurrentLocationToBackHistory();
        });
    }//EoM setJExplorerNavigationForwardButtonOnClickEvents-------------------------------------------------------------

    private void setJExplorerNavigationBarOnUserValue() {
        this.getjExplorerNavigationBar().setOnKeyPressed(event -> {
            if(event.getCode() == KeyCode.ENTER){
                File file = new File(this.getjExplorerNavigationBar().getValue());
                if(file.exists()) {
                    JExplorerFile userFileValueFromNavigation = new JExplorerFile(this.getjExplorerNavigationBar().getValue());
                    JExplorerCenterFileExplorerController.getInstance().openFileOrExpandDirectory(userFileValueFromNavigation);
                }
            }
        });
    }//EoM setJExplorerNavigationBarOnUserValue-------------------------------------------------------------------------

    //Getters and Setters
    public JExplorerNavigationBackButton getJExplorerNavigationBackButton() {
        return jExplorerNavigationBackButton;
    }

    public void setJExplorerNavigationBackButton(JExplorerNavigationBackButton jExplorerNavigationBackButton) {
        this.jExplorerNavigationBackButton = jExplorerNavigationBackButton;
        setJExplorerNavigationBackButtonOnClickEvents();
    }
    private void setJExplorerNavigationBackButtonOnClickEvents(){
        this.getJExplorerNavigationBackButton().setOnMouseClicked(event -> {
            this.addCurrentLocationToForwardHistory();
        });
    }

    public JExplorerNavigationForwardButton getJExplorerNavigationForwardButton() {
        return jExplorerNavigationForwardButton;
    }

    public void setJExplorerNavigationForwardButton(JExplorerNavigationForwardButton jExplorerNavigationForwardButton) {
        this.jExplorerNavigationForwardButton = jExplorerNavigationForwardButton;
        setJExplorerNavigationForwardButtonOnClickEvents();
    }

    public JExplorerNavigationHistory getJExplorerNavigationHistory() {
        return jExplorerNavigationHistory;
    }

    public void setJExplorerNavigationHistory(JExplorerNavigationHistory jExplorerNavigationHistory) {
        this.jExplorerNavigationHistory = jExplorerNavigationHistory;
    }

    public boolean isFirstPopFrontSide() {
        return isFirstPopFrontSide;
    }

    public void setFirstPopFrontSide(boolean firstPopFrontSide) {
        isFirstPopFrontSide = firstPopFrontSide;
    }

    public boolean isFirstPopBackSide() {
        return isFirstPopBackSide;
    }

    public void setFirstPopBackSide(boolean firstPopBackSide) {
        isFirstPopBackSide = firstPopBackSide;
    }

    private void setListenerForFileNavigation(){
        NavigationListener navigationListener = new NavigationListener(this.getjExplorerCenterFileExplorerTableView(), this.getJExplorerNavigationHistory());
        this.getJExplorerNavigationHistory().addPropertyChangeListener(navigationListener);
    }

    public JExplorerCenterFileExplorerTableView getjExplorerCenterFileExplorerTableView() {
        return jExplorerCenterFileExplorerTableView;
    }

    public void setjExplorerCenterFileExplorerTableView(JExplorerCenterFileExplorerTableView jExplorerCenterFileExplorerTableView) {
        this.jExplorerCenterFileExplorerTableView = jExplorerCenterFileExplorerTableView;
        this.setListenerForFileNavigation();
    }

    public JExplorerNavigationBar getjExplorerNavigationBar() {
        return jExplorerNavigationBar;
    }

    public void setjExplorerNavigationBar(JExplorerNavigationBar jExplorerNavigationBar) {
        this.jExplorerNavigationBar = jExplorerNavigationBar;
        setJExplorerNavigationBarOnUserValue();
    }//End of Getters and Setters---------------------------------------------------------------------------------------

    private class NavigationListener implements PropertyChangeListener {

        JExplorerCenterFileExplorerTableView jExplorerCenterFileExplorerTableView;
        JExplorerNavigationHistory jExplorerNavigationHistory;

        public NavigationListener(JExplorerCenterFileExplorerTableView jExplorerCenterFileExplorerTableView, JExplorerNavigationHistory jExplorerNavigationHistory){
            this.jExplorerCenterFileExplorerTableView = jExplorerCenterFileExplorerTableView;
            this.jExplorerNavigationHistory = jExplorerNavigationHistory;
        }

        @Override
        public void propertyChange(PropertyChangeEvent event) {
            if (event.getPropertyName().equals("setPoppedFile")) {
                File[] files = new File(this.jExplorerNavigationHistory.getPoppedFile().getAbsolutePath()).listFiles();
                JExplorerCenterFileExplorerController.getInstance().getData().clear();
                if (files != null) {
                    for(File file: files){
                        if(!file.getName().equals("desktop.ini"))
                        JExplorerCenterFileExplorerController.getInstance().getData().add(new JExplorerFile(file.getAbsolutePath()));
                    }
                }
                this.jExplorerCenterFileExplorerTableView.setOnMouseClicked(event2 -> {
                    if(event2.getClickCount() == 2){
                        JExplorerFile fileDirectoryDoubleClicked = (JExplorerFile) this.jExplorerCenterFileExplorerTableView.getSelectionModel().getSelectedItem();
                        JExplorerCenterFileExplorerController.getInstance().openFileOrExpandDirectory(fileDirectoryDoubleClicked);
                    }
                });
                jExplorerCenterFileExplorerTableView.getColumns().setAll(new CenterColumns());
                jExplorerCenterFileExplorerTableView.setItems(JExplorerCenterFileExplorerController.getInstance().getData());//Set this view
            }
        }
    }

}
