package jExplorerControllers.jExplorerTopControllers;

import jExplorerView.jExplorerContainers.MainStage;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTitleBar.JExplorerTitleBarHBox;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.JExplorerWindowsTabTabPane;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.TabSelected;
import jExplorerView.utility.ChildrenStageHandler;
import jExplorerView.utility.WindowMaximizer;
import jExplorerView.utility.WindowResizer;

public class JExplorerTitleBarController {

    private final JExplorerTitleBarController.DragDelta dragDelta = new JExplorerTitleBarController.DragDelta();
    private JExplorerTitleBarHBox jExplorerTitleBarHBox;
    private WindowMaximizer windowMaximizer;

    public JExplorerTitleBarController(JExplorerTitleBarHBox jExplorerTitleBarHBox){
        this.jExplorerTitleBarHBox = jExplorerTitleBarHBox;
        setWindowMaximizer(new WindowMaximizer());
    }

    public void setMouseEvents(){
        jExplorerTitleBarHBox.setOnMousePressed(event ->  {
            ChildrenStageHandler.getInstance().hideAllStagesInList();
            JExplorerWindowsTabTabPane.getInstance().getSelectionModel().select(TabSelected.NO_SELECT.getValue());
            dragDelta.x = MainStage.getPrimaryStage().getX() - event.getScreenX();
            dragDelta.y = MainStage.getPrimaryStage().getY() - event.getScreenY();
        });

        jExplorerTitleBarHBox.setOnMouseClicked(event -> {
            if(event.getClickCount() == 2){
                if(!windowMaximizer.isMaximized()){
                    windowMaximizer.maximizeWindow();
                }else{
                    windowMaximizer.undoWindowMaximize();
                }
            }
        });

        jExplorerTitleBarHBox.setOnMouseDragged(event -> {
            if(windowMaximizer.isMaximized()){
                windowMaximizer.undoWindowMaximize();
            }
            if(!WindowResizer.isLock()){
                MainStage.getPrimaryStage().setX(event.getScreenX() + dragDelta.x);
                MainStage.getPrimaryStage().setY(event.getScreenY() + dragDelta.y);
            }
        });
    }

    public WindowMaximizer getWindowMaximizer(){
        return this.windowMaximizer;
    }

    public void setWindowMaximizer(WindowMaximizer windowMaximizer) {
        this.windowMaximizer = windowMaximizer;
    }

    private class DragDelta{
        double x, y;
    }
}
