package jExplorerControllers.jExplorerTopControllers;

import jExplorerControllers.jExplorerNavigationControllers.JExplorerNavigationController;
import jExplorerView.jExplorerContainers.MainStage;
import javafx.scene.input.KeyCode;

public class JExplorerHotKeyController {

    public JExplorerHotKeyController(){
        MainStage.getPrimaryStage().getScene().setOnKeyPressed(event -> {
            if(event.getCode() == KeyCode.BACK_SPACE || event.getCode() == KeyCode.LEFT && event.isAltDown()){
                JExplorerNavigationController.getInstance().addCurrentLocationToForwardHistory();
            } else if(event.getCode() == KeyCode.RIGHT && event.isAltDown()){
                JExplorerNavigationController.getInstance().addCurrentLocationToBackHistory();
            }
        });
    }
}
