package jExplorerControllers.jExplorerTopControllers;

import jExplorerView.jExplorerContainers.MainStage;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTitleBar.jExplorerTitleBarButtons.JExplorerTitleBarButton;
import JExplorerUtility.JExplorerPropertiesWriter;
import jExplorerView.utility.UserInfo;
import jExplorerView.utility.WindowMaximizer;
import javafx.scene.control.Button;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class JExplorerTitleBarButtonController {
    private static final Logger LOGGER = LogManager.getLogger("JExplorer");

    private JExplorerTitleBarButton titleBarButton;
    private WindowMaximizer windowMaximizer;

    public JExplorerTitleBarButtonController(JExplorerTitleBarButton titleBarButton){
        this.titleBarButton = titleBarButton;
    }

    public void setMinimizeButtonMouseEvents(){
        this.titleBarButton.setOnMouseClicked(minimizeEvent -> {
            MainStage.getPrimaryStage().setIconified(true);
        });
    }

    public void setMaximizeButtonMouseEvents(){
        this.titleBarButton.setOnMouseClicked(event -> {
            if(!this.getWindowMaximizer().isMaximized()){
                this.getWindowMaximizer().maximizeWindow();
            }else{
                this.getWindowMaximizer().undoWindowMaximize();
            }
        });
    }
    public void setListenerForWindowMaximization(){
        MaximizedListener maximizedChecker = new MaximizedListener(this.titleBarButton, this.windowMaximizer);
        getWindowMaximizer().addPropertyChangeListener(maximizedChecker);
    }

    public void setCloseButtonMouseEvents(){
        this.titleBarButton.setOnMouseClicked(closeEvent -> {
            JExplorerPropertiesWriter jExplorerPropertiesWriter = new JExplorerPropertiesWriter(LOGGER, MainStage.getPrimaryStage());
            jExplorerPropertiesWriter.setDirectoryToWriteCustomFile("C:\\Users\\" + UserInfo.getInstance().getUserName() + "\\JExplorer");
            jExplorerPropertiesWriter.setFileNameToWriteDimensions("JExplorer.properties");
            jExplorerPropertiesWriter.savePropertiesToFileAndCloseApplication();
        });
    }

    public WindowMaximizer getWindowMaximizer() {
        return windowMaximizer;
    }

    public void setWindowMaximizer(WindowMaximizer windowMaximizer) {
        this.windowMaximizer = windowMaximizer;
    }

    private class MaximizedListener implements PropertyChangeListener {
        private Button button;
        private WindowMaximizer windowMaximizer;

        public MaximizedListener(Button closeButton, WindowMaximizer windowMaximizer){
            this.button = closeButton;
            this.windowMaximizer = windowMaximizer;
        }

        @Override
        public void propertyChange(PropertyChangeEvent event) {
            if (event.getPropertyName().equals("IsMaximized")) {
                if(this.windowMaximizer.isMaximized()){
                    button.setText("\u29C9");
                }else{
                    button.setText("\u2610");
                }
            }
        }
    }
}

