package jExplorerView.jExplorerContainers;

import javafx.scene.layout.BorderPane;
import jExplorerView.jExplorerContainers.jExplorerCenterBorderPane.JExplorerCenterFileExplorerTableView;
import jExplorerView.jExplorerContainers.jExplorerCenterBorderPane.JExplorerHBoxCenter;
import jExplorerView.jExplorerContainers.jExplorerBottomBorderPane.JExplorerBottomHBox;
import jExplorerView.jExplorerContainers.jExplorerLeftBorderPane.JExplorerLeftSideExplorerTreeView;
import jExplorerView.jExplorerContainers.jExplorerLeftBorderPane.JExplorerLeftSideHBox;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.JExplorerTopToolBarVBox;
import jExplorerView.utility.WindowResizer;

/*
 * Purpose - This class creates a JavaFx BorderPane and populates its regions with JExplorer Nodes.
 *
 * @author Matthew Williams
 * @since 2018-08-11
 */

public class JExplorerBorderPane extends BorderPane{

    /*
     * Purpose - This constructor creates an Fx BorderPane with custom resizing functionality.  Additionally, it
     *           populates itself with JExplorer Nodes.
     *
     *
     */
    public JExplorerBorderPane(){

        //Window minimum size and resize threshold
        double MIN_RESIZE_HEIGHT = 230;
        double MIN_RESIZE_WIDTH = 280;
        int RESIZE_THRESHOLD = 3;

        //Outer border of the window customization
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerBorderPane");
        this.populateBorderPaneRegions();

        //Controls resizing the window
        new WindowResizer(this, MIN_RESIZE_WIDTH, MIN_RESIZE_HEIGHT, RESIZE_THRESHOLD);
    }//EoM JExplorerBorderPane Constructor---------------------------------------------------------------------------------

    /*
     *  Purpose: This method populates the areas(top, left, center, right, bottom) of a
     *           JExplorerBorderPane with fx nodes.  Below is crude image of the areas of a
     *           customBorderPane.
     *   _______________
     *   |______T______|
     *   | |         | |
     *   |L|    C    |R|
     *   | |         | |
     *   | |_________| |
     *   |______B______|
     *
     */
    private void populateBorderPaneRegions() {

        //Top pane
        super.setTop(new JExplorerTopToolBarVBox());

        //Create explorerTreeView and tableView for left and center pane
        JExplorerLeftSideExplorerTreeView jExplorerLeftSideExplorerTreeView = new JExplorerLeftSideExplorerTreeView();
        JExplorerCenterFileExplorerTableView jExplorerCenterFileExplorerTableView = new JExplorerCenterFileExplorerTableView(jExplorerLeftSideExplorerTreeView);
        JExplorerBottomHBox customBottomRow = new JExplorerBottomHBox(jExplorerCenterFileExplorerTableView);

        super.setLeft(new JExplorerLeftSideHBox(jExplorerLeftSideExplorerTreeView));
        super.setCenter(new JExplorerHBoxCenter(jExplorerCenterFileExplorerTableView));
        super.setBottom(customBottomRow);
    }
    //EoM populateBorderPaneRegions-------------------------------------------------------------------------------------
}//EoC JExplorerBorderPane-------------------------------------------------------------------------------------------------
