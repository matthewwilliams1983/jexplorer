package jExplorerView.jExplorerContainers;

import jExplorerControllers.jExplorerTopControllers.JExplorerHotKeyController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import JExplorerUtility.JExplorerPropertiesReader;

public class MainStage extends Application {

    private static Stage primaryStage;
    private JExplorerHotKeyController jExplorerHotKeyController;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        setPrimaryStage(primaryStage);

        //Create border pane
        BorderPane windowBorder = new JExplorerBorderPane();

        //Have the window be the same size the user closed the program at last
        JExplorerPropertiesReader JExplorerPropertiesReader = new JExplorerPropertiesReader();
        Scene primaryScene = new Scene(windowBorder, JExplorerPropertiesReader.getPreviousWindowWidthPref(),
                JExplorerPropertiesReader.getPreviousWindowHeightPref());
        primaryScene.getStylesheets().add("css/defaultTheme.css");

        //Setup and show primaryStage
        setupPrimaryStage(primaryScene);

        jExplorerHotKeyController = new JExplorerHotKeyController();
        System.setProperty("jExplorer.home", System.getProperty("user.home"));
    }

    private void setupPrimaryStage(Scene primaryScene) {
        getPrimaryStage().initStyle(StageStyle.UNDECORATED);//Override system title bar
        getPrimaryStage().setTitle("JExplorer");
        getPrimaryStage().getIcons().add(new Image(ClassLoader.getSystemResourceAsStream("images/blackFolder50x50.png")));
        getPrimaryStage().setScene(primaryScene);
        getPrimaryStage().show();
    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void setPrimaryStage(Stage primaryStage) {
        MainStage.primaryStage = primaryStage;
    }


}
