package jExplorerView.jExplorerContainers.jExplorerCenterBorderPane;

import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class JExplorerHBoxCenter extends HBox {
    public JExplorerHBoxCenter(JExplorerCenterFileExplorerTableView jExplorerCenterFileExplorerTableView){
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerHBoxCenter");
        HBox.setHgrow(jExplorerCenterFileExplorerTableView, Priority.ALWAYS);
        super.getChildren().add(jExplorerCenterFileExplorerTableView);
        super.getChildren().add(new JExplorerCenterEndRegionSpacer());
    }
}
