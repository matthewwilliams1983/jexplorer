package jExplorerView.jExplorerContainers.jExplorerCenterBorderPane.columns;

import java.util.ArrayList;

public class CenterColumns extends ArrayList<CenterTableViewColumn>{

    public CenterColumns(){//TODO Have columns resize to fit content
        this.add(new CenterTableViewColumn("Name", "fileIconWithName", "fileIconWithName"));
        this.add(new CenterTableViewColumn("Date modified", "lastModified", "dateModifiedColumn"));
        this.add(new CenterTableViewColumn("Type", "fileType", "typeColumn"));
        this.add(new CenterTableViewColumn("Size", "fileSize", "sizeColumn"));
    }
}
