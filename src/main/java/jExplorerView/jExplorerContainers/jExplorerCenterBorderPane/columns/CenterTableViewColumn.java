package jExplorerView.jExplorerContainers.jExplorerCenterBorderPane.columns;

import javafx.scene.control.cell.PropertyValueFactory;
import jExplorerData.jExplorerDirectoryUtility.JExplorerFile;
import javafx.scene.control.TableColumn;

public class CenterTableViewColumn<T> extends TableColumn <JExplorerFile,T>{
    public CenterTableViewColumn(String columnName, String propertyValue, String id){
        super(columnName);
        this.setId(id);
        this.setCellValueFactory(new PropertyValueFactory<>(propertyValue));
    }
}
