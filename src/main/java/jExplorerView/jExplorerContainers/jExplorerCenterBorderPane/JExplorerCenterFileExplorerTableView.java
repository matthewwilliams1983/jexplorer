package jExplorerView.jExplorerContainers.jExplorerCenterBorderPane;

import jExplorerControllers.JExplorerStartViewController;
import jExplorerControllers.jExplorerNavigationControllers.JExplorerCenterFileExplorerController;
import jExplorerControllers.jExplorerNavigationControllers.JExplorerNavigationController;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import jExplorerView.jExplorerContainers.jExplorerLeftBorderPane.JExplorerLeftSideExplorerTreeView;

/*
 * Purpose - This class is the TableView of the JExplorer.  It holds the list of the all the files which is the same
 *           jExplorerData that is generated from side table view (left side of the customSideExplorerTreeView).  It handles
 *           double click events to expand the view to show the contents of the directory being double clicked on or
 *           opening of file if it was not a directory. See drawing below...
 *   _______________
 *   |______T______|
 *   | |         | |
 *   | |   This  |R|
 *   | |         | |
 *   | |_________| |
 *   |______B______|
 *
 * @author Matthew Williams
 * @since 2018-08-11
 */

public class JExplorerCenterFileExplorerTableView extends TableView {

    /*
     * Purpose - This constructor creates a TableView that contains the subdirectories of the selection from a
     *           JExplorerLeftSideExplorerTreeView.
     * @param customSideExplorerTreeView - The TreeView whose selection will determine how to populate this
     *                                     TableView's jExplorerData.
     */

    public JExplorerCenterFileExplorerTableView(JExplorerLeftSideExplorerTreeView jExplorerLeftSideExplorerTreeView){

        super.setId("jExplorerCenterFileExplorerTableView");
        super.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //Controllers
        JExplorerNavigationController.getInstance().setjExplorerCenterFileExplorerTableView(this);
        JExplorerCenterFileExplorerController.getInstance().setjExplorerCenterFileExplorerTableView(this);
        JExplorerCenterFileExplorerController.getInstance().setjExplorerLeftSideExplorerTreeView(jExplorerLeftSideExplorerTreeView);
        JExplorerCenterFileExplorerController.getInstance().setStartView();
    }

}//EoC JExplorerCenterFileExplorerTableView-----------------------------------------------------------------------------
