package jExplorerView.jExplorerContainers.jExplorerCenterBorderPane;

import javafx.scene.layout.Region;

public class JExplorerCenterEndRegionSpacer extends Region {
    public JExplorerCenterEndRegionSpacer(){
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerCenterEndRegionSpacer");
    }
}
