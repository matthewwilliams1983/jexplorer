package jExplorerView.jExplorerContainers.jExplorerRightBorderPane;

import javafx.scene.layout.VBox;
/*
 * Purpose - This class is the VBox that will hold plugins.  Currently this is just a placeholder.
 *
 */

public class JExplorerRightVBox extends VBox {
    public JExplorerRightVBox(){
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerRightVBox");
    }
}
