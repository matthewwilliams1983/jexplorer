package jExplorerView.jExplorerContainers.jExplorerBottomBorderPane;

import javafx.scene.control.Label;

public class JExplorerItemAndSelectionLabel extends Label {

    private String itemCountLabelText;
    private String selectionCountLabelText;

    public JExplorerItemAndSelectionLabel(){
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerItemAndSelectionLabel");
        this.setItemCountLabelText("");
        this.setSelectionCountLabelText("");
    }

    public String getItemCountLabelText() {
        return itemCountLabelText;
    }

    public void setItemCountLabelText(String itemCountLabelText) {
        this.itemCountLabelText = itemCountLabelText;
    }

    public String getSelectionCountLabelText() {
        return selectionCountLabelText;
    }

    public void setSelectionCountLabelText(String selectionCountLabelText) {
        this.selectionCountLabelText = selectionCountLabelText;
    }
}
