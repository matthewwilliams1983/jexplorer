package jExplorerView.jExplorerContainers.jExplorerBottomBorderPane;

import jExplorerControllers.jExplorerNavigationControllers.JExplorerCenterFileExplorerController;
import javafx.collections.ListChangeListener;
import javafx.scene.layout.HBox;
import jExplorerView.jExplorerContainers.jExplorerCenterBorderPane.JExplorerCenterFileExplorerTableView;
import jExplorerData.jExplorerDirectoryUtility.JExplorerFile;

/*
 * Purpose - This class is an HBox node that contains information about selection.
 *
 * @author - Matthew Williams
 * @since 2018-08-11
 */

public class JExplorerBottomHBox extends HBox{
    /*
     * Purpose - This constructor populates itself with label indicating that is reserved for future content
     *          (placeholder). See image below of the customBorderPane
     *   _______________
     *   |______T______|
     *   | |         | |
     *   |L|    C    |R|
     *   | |         | |
     *   | |_________| |
     *   |____This_____|
     */
    private JExplorerItemAndSelectionLabel itemAndSelectionCountLabel;

    public JExplorerBottomHBox(JExplorerCenterFileExplorerTableView JExplorerCenterFileExplorerTableView){

        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerBottomHBox");

        itemAndSelectionCountLabel = new JExplorerItemAndSelectionLabel();

        super.getChildren().add(itemAndSelectionCountLabel);

        addListenersToTableView(JExplorerCenterFileExplorerTableView);
    }//EoM JExplorerBottomHBox Constructor----------------------------------------------------------------------------------

    private void addListenersToTableView(JExplorerCenterFileExplorerTableView JExplorerCenterFileExplorerTableView) {
        JExplorerCenterFileExplorerController.getInstance().getData().addListener((ListChangeListener<JExplorerFile>) c -> {
            itemAndSelectionCountLabel.setItemCountLabelText("     " + JExplorerCenterFileExplorerController.getInstance().getData().size() + " items     ");
            itemAndSelectionCountLabel.setText(itemAndSelectionCountLabel.getItemCountLabelText() + itemAndSelectionCountLabel.getSelectionCountLabelText());
        });
        JExplorerCenterFileExplorerTableView.getSelectionModel().getSelectedCells().addListener((ListChangeListener<JExplorerFile>) c -> {
            int selectionCount = JExplorerCenterFileExplorerTableView.getSelectionModel().getSelectedCells().size();
            if(selectionCount == 0){
                itemAndSelectionCountLabel.setSelectionCountLabelText("");
            }else{
                itemAndSelectionCountLabel.setSelectionCountLabelText(selectionCount + " items selected");
            }
            itemAndSelectionCountLabel.setText(itemAndSelectionCountLabel.getItemCountLabelText() + itemAndSelectionCountLabel.getSelectionCountLabelText());
        });
    }
    //EoG&S JExplorerBottomHBox---------------------------------------------------------------------------------------------
}//EoC JExplorerBottomHBox--------------------------------------------------------------------------------------------------
