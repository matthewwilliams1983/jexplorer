package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationButtons;

import jExplorerControllers.jExplorerNavigationControllers.JExplorerNavigationController;
import javafx.scene.control.Button;

public class JExplorerNavigationForwardButton extends Button {
    public JExplorerNavigationForwardButton(){
        super.setId("jExplorerNavigationForwardButton");
        super.setText("\u27A1");
        JExplorerNavigationController.getInstance().setJExplorerNavigationForwardButton(this);
    }
}
