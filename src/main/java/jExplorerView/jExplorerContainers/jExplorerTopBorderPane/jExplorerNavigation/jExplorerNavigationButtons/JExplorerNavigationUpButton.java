package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationButtons;

import javafx.scene.control.Button;

public class JExplorerNavigationUpButton extends Button {
    public JExplorerNavigationUpButton(){
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerNavigationUpButton");
        super.setText("\u2B06");
    }
}
