package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationButtons;

import jExplorerControllers.jExplorerNavigationControllers.JExplorerNavigationController;
import javafx.scene.control.Button;

public class JExplorerNavigationBackButton extends Button {
    public JExplorerNavigationBackButton(){
        super.setId("jExplorerNavigationBackButton");
        super.setText("\u2B05");
        JExplorerNavigationController.getInstance().setJExplorerNavigationBackButton(this);
    }
}
