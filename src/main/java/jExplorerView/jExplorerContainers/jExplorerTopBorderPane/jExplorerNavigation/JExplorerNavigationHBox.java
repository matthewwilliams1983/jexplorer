package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation;

import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationBar.JExplorerNavigationBar;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationBar.JExplorerNavigationBarRefreshButton;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerSearchBar.JExplorerSearchBarTextField;
import jExplorerView.utility.JExplorerHorizontalRegionSpacer;
import javafx.scene.layout.HBox;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationButtons.JExplorerNavigationBackButton;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationButtons.JExplorerNavigationForwardButton;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationButtons.JExplorerNavigationUpButton;

public class JExplorerNavigationHBox extends HBox {
    public JExplorerNavigationHBox(){
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerNavigation");
        super.getChildren().addAll(new JExplorerNavigationBackButton(), new JExplorerNavigationForwardButton(),
                new JExplorerNavigationUpButton(), new JExplorerNavigationBar(), new JExplorerNavigationBarRefreshButton(),
        new JExplorerHorizontalRegionSpacer(10), new JExplorerSearchBarTextField());
    }
}
