package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerSearchBar;

import javafx.scene.control.TextField;

public class JExplorerSearchBarTextField extends TextField {
    public JExplorerSearchBarTextField(){
        super.setId("jExplorerSearchBarTextField");
        super.setText("Search This Location");
    }
}
