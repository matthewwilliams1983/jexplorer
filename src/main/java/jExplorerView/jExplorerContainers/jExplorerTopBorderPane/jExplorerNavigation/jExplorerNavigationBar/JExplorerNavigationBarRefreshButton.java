package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationBar;

import javafx.scene.control.Button;

public class JExplorerNavigationBarRefreshButton extends Button {
    public JExplorerNavigationBarRefreshButton(){
        super.setId("jExplorerNavigationBarRefreshButton");
        super.setText("\u21bb");
    }
}
