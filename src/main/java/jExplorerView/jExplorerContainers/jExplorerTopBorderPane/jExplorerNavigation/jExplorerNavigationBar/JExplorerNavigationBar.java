package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.jExplorerNavigationBar;

import jExplorerControllers.jExplorerNavigationControllers.JExplorerCenterFileExplorerController;
import jExplorerControllers.jExplorerNavigationControllers.JExplorerNavigationController;
import javafx.scene.control.ComboBox;

public class JExplorerNavigationBar extends ComboBox<String> {
    public JExplorerNavigationBar(){
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerNavigationBar");
        super.setEditable(true);
        JExplorerNavigationController.getInstance().setjExplorerNavigationBar(this);
        JExplorerCenterFileExplorerController.getInstance().setjExplorerNavigationBar(this);
    }
}
