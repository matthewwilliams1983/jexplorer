package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptions;

import javafx.scene.layout.VBox;

public class FileTabVBox extends VBox {

    public FileTabVBox(){
        this.setId("fileTabVBox");
        this.getStylesheets().add("css/defaultTheme.css");
        this.getChildren().addAll(new FileTabOptionButtons());
    }
}
