package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.JExplorerWindowsTabTabPane;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.TabSelected;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptionButtonExpandable;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class FileTabOptionButtonHelp extends FileTabOptionButtonExpandable {

    private static final Logger LOGGER = LogManager.getLogger("JExplorer");
    private String urlToHelp;

    public FileTabOptionButtonHelp(String idName, String buttonText, boolean isExpandable) {
        super(idName, buttonText, isExpandable);
        setUrlToHelp("https://google.com");
    }
    @Override
    protected void createOnClickAction() {
        this.setOnMouseClicked(event -> {
            try {
                JExplorerWindowsTabTabPane.getInstance().getSelectionModel().select(TabSelected.NO_SELECT.getValue());
                Desktop.getDesktop().browse(new URL(getUrlToHelp()).toURI());
            } catch (IOException | URISyntaxException e) {
                LOGGER.error(this.getClass() + ": " + e.toString());
            }
        });
    }

    public String getUrlToHelp() {
        return urlToHelp;
    }

    public void setUrlToHelp(String urlToHelp) {
        this.urlToHelp = urlToHelp;
    }
}
