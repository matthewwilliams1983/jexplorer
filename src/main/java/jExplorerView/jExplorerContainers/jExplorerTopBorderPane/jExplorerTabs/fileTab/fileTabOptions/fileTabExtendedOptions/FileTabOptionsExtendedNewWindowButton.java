package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.fileTabExtendedOptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.JExplorerWindowsTabTabPane;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.TabSelected;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptionButtonNonExpandable;

import java.io.IOException;

public class FileTabOptionsExtendedNewWindowButton extends FileTabOptionButtonNonExpandable {

    private static final Logger LOGGER = LogManager.getLogger("JExplorer");

    public FileTabOptionsExtendedNewWindowButton(String idName, String buttonText, boolean isExpandable) {
        super(idName, buttonText, isExpandable);
    }

    @Override
    protected void createOnClickAction() {
        boolean isDev = true;
        this.setOnMouseClicked(event -> {
            this.getScene().getWindow().hide();
            Runtime runtime = Runtime.getRuntime();
            try {
                if(isDev){
                    JExplorerWindowsTabTabPane.getInstance().getSelectionModel().select(TabSelected.NO_SELECT.getValue());
                    runtime.exec("C:\\Users\\Matt\\Desktop\\JavaProjects\\IntelliJIdeaProjects\\customFileExplorer\\target\\JExplorer.exe");
                }
                else
                    runtime.exec(System.getProperty("user.dir") + "/JExplorer.exe");
            } catch (IOException e) {
                LOGGER.error(this.getClass() + ": " + e.toString());
            }
        });
    }
}
