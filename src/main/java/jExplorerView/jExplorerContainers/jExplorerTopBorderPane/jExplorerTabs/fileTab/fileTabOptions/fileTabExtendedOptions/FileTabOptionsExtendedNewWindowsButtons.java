package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.fileTabExtendedOptions;

import javafx.scene.Node;

import java.util.ArrayList;

public class FileTabOptionsExtendedNewWindowsButtons extends ArrayList<Node> {
    public FileTabOptionsExtendedNewWindowsButtons(){
        this.add(new FileTabOptionsExtendedNewWindowButton("extendedFileTabOptionButtons","Open new window", false));
        this.add(new FileTabOptionsExtendedNewProcessWindowButton("extendedFileTabOptionButtons","Open new window in new process", false));
    }
}
