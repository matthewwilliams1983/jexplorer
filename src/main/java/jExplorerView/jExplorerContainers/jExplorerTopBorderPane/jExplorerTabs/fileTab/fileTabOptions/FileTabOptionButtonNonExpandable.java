package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions;

public abstract class FileTabOptionButtonNonExpandable extends FileTabOptionButton {
    public FileTabOptionButtonNonExpandable(String idName, String buttonText, boolean isExpandable) {
        super(idName, buttonText, isExpandable);
        createOnClickAction();
    }

    protected abstract void createOnClickAction();
}
