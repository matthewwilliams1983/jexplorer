package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions;

import javafx.scene.control.Button;

public abstract class FileTabOptionButton extends Button {

    private boolean isExpandable;

    public FileTabOptionButton(String idName, String buttonText, boolean isExpandable){

        this.setId(idName);
        this.getStylesheets().add("css/defaultTheme.css");
        this.setText(buttonText);
        this.setExpandable(isExpandable);
    }

    public boolean isExpandable() {
        return isExpandable;
    }

    public void setExpandable(boolean expandable) {
        isExpandable = expandable;
    }

}
