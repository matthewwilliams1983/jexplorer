package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions;

import javafx.application.Platform;
import jExplorerView.jExplorerContainers.MainStage;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.fileTabExtendedOptions.FileOptionsExtendedStage;

public abstract class FileTabOptionButtonExpandable extends FileTabOptionButton {

    private FileOptionsExtendedStage fileOptionExtendedStage;

    public FileTabOptionButtonExpandable(String idName, String buttonText, boolean isExpandable) {
        super(idName, buttonText, isExpandable);
        createOnClickAction();
        setFileOptionExtendedStage(new FileOptionsExtendedStage(this.getText(), this));
        setMouseEventsExpandable();
    }

    private void setMouseEventsExpandable() {
        this.setOnMouseEntered(event ->{
            showFileTabOptionsExtendedWindow();
        });
        this.setOnMouseExited(event -> {
            checkToCloseFileTabOptionsExtendedWindow();
        });
    }

    private void showFileTabOptionsExtendedWindow() {
        if(!getFileOptionExtendedStage().isShowing()){
            Platform.runLater(() ->{
                getFileOptionExtendedStage().setX(MainStage.getPrimaryStage().getX() + 275);
                getFileOptionExtendedStage().setY(MainStage.getPrimaryStage().getY() + 60);
                getFileOptionExtendedStage().show();
            });
        }
    }

    private void checkToCloseFileTabOptionsExtendedWindow() {
        if(getFileOptionExtendedStage().isShowing() && !getFileOptionExtendedStage().isFileOptionVBoxHover()){
            Platform.runLater(()->{
                getFileOptionExtendedStage().hide();
            });
        }
    }

    protected abstract void createOnClickAction();

    public FileOptionsExtendedStage getFileOptionExtendedStage() {
        return fileOptionExtendedStage;
    }

    public void setFileOptionExtendedStage(FileOptionsExtendedStage fileOptionExtendedStage) {
        this.fileOptionExtendedStage = fileOptionExtendedStage;
    }
}
