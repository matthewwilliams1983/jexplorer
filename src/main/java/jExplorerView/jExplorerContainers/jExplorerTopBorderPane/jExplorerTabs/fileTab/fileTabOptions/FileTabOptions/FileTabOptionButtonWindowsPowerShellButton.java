package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.JExplorerWindowsTabTabPane;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.TabSelected;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptionButtonNonExpandable;

import java.io.IOException;

public class FileTabOptionButtonWindowsPowerShellButton extends FileTabOptionButtonNonExpandable {

    private static final Logger LOGGER = LogManager.getLogger("JExplorer");

    public FileTabOptionButtonWindowsPowerShellButton(String idName, String buttonText, boolean isExpandable) {
        super(idName, buttonText, isExpandable);
    }

    @Override
    protected void createOnClickAction() {
        this.setOnMouseClicked(event -> {
            Runtime runtime = Runtime.getRuntime();
            try {
                JExplorerWindowsTabTabPane.getInstance().getSelectionModel().select(TabSelected.NO_SELECT.getValue());
                String[] str = { "cmd",  "/c", "start", "powershell.exe", "-NoExit", "-Command", "cd",  System.getProperty("user.home")};
                runtime.exec(str);
            } catch (IOException e) {
                LOGGER.error(this.getClass() + ": " + e.toString());
            }
        });
    }
}
