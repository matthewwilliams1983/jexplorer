package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.fileTabExtendedOptions;

import javafx.scene.Node;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class FileTabExtendedOptionsVBox extends VBox {
    public FileTabExtendedOptionsVBox(ArrayList<Node> nodeList){
        this.setId("fileOptionsVBox");
        this.getStylesheets().add("css/defaultTheme.css");
        this.getChildren().addAll(nodeList);
    }
}
