package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.fileTabExtendedOptions;

import javafx.scene.Node;

import java.util.ArrayList;

public class FileTabOptionsExtendedHelpButtons extends ArrayList<Node> {
    public FileTabOptionsExtendedHelpButtons(){
        this.add(new FileTabOptionsExtendedHelpButton("extendedFileTabOptionButtons","Help", false));
        this.add(new FileTabOptionsExtendedAboutButton("extendedFileTabOptionButtons","About", false));
    }
}
