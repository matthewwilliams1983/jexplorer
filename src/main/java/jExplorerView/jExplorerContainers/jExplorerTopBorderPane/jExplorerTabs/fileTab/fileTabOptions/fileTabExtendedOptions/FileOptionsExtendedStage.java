package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.fileTabExtendedOptions;

import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import jExplorerView.jExplorerContainers.MainStage;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptionButtonExpandable;
import jExplorerView.utility.ChildrenStageHandler;

public class FileOptionsExtendedStage extends Stage{

    private FileTabExtendedOptionsVBox fileTabExtendedOptionsVBox;
    private FileTabOptionButtonExpandable parentButton;

    public FileOptionsExtendedStage(String extendedOptionsButtonId, FileTabOptionButtonExpandable parentButton){
        setParentButton(parentButton);
        ChildrenStageHandler.getInstance().addStageToOpenStageList(this);
        switch (extendedOptionsButtonId) {
            case "Open new window":
                setupNewWindowExtendedOptions();
                break;
            case "Open Windows PowerShell":
                break;
            case "Change folders and search options":
                break;
            case "Help":
                setupHelpExtendedOptions();
                break;
            case "Close":
                break;
        }
    }

    private void setupNewWindowExtendedOptions() {
        this.initStyle(StageStyle.UNDECORATED);
        this.initModality(Modality.NONE);
        this.initOwner(MainStage.getPrimaryStage());
        fileTabExtendedOptionsVBox = new FileTabExtendedOptionsVBox(new FileTabOptionsExtendedNewWindowsButtons());
        fileTabExtendedOptionsVBox.setOnMouseExited(event -> {
            if(!this.getParentButton().isHover())
                this.hide();
        });
        Scene fileOptionExtendedScene = new Scene(fileTabExtendedOptionsVBox, 275, 236);
        this.setScene(fileOptionExtendedScene);
    }

    private void setupHelpExtendedOptions() {
        this.initStyle(StageStyle.UNDECORATED);
        this.initModality(Modality.NONE);
        this.initOwner(MainStage.getPrimaryStage());
        fileTabExtendedOptionsVBox = new FileTabExtendedOptionsVBox(new FileTabOptionsExtendedHelpButtons());
        fileTabExtendedOptionsVBox.setOnMouseExited(event -> {
            if(!this.getParentButton().isHover())
                this.hide();
        });
        Scene fileOptionExtendedScene = new Scene(fileTabExtendedOptionsVBox, 275, 236);
        this.setScene(fileOptionExtendedScene);
    }

    public boolean isFileOptionVBoxHover(){
        return fileTabExtendedOptionsVBox.isHover();
    }

    public FileTabOptionButtonExpandable getParentButton() {
        return parentButton;
    }

    public void setParentButton(FileTabOptionButtonExpandable parentButton) {
        this.parentButton = parentButton;
    }
}
