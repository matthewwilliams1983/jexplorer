package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptions;

import javafx.application.Platform;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptionButtonNonExpandable;

public class FileTabOptionButtonClose extends FileTabOptionButtonNonExpandable {
    public FileTabOptionButtonClose(String idName, String buttonText, boolean isExpandable) {
        super(idName, buttonText, isExpandable);
    }

    @Override
    protected void createOnClickAction() {
        this.setOnMouseClicked(event -> {
            Platform.exit();
        });
    }
}
