package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptions;

import javafx.scene.Node;
import jExplorerView.utility.CustomLine;

import java.util.ArrayList;

public class FileTabOptionButtons extends ArrayList<Node> {
    public FileTabOptionButtons(){
        this.add(new FileTabOptionOpenNewWindowButton("newWindow","Open new window", true));
        this.add(new FileTabOptionButtonWindowsPowerShellButton("windowsPowerShell","Open Windows PowerShell", false));
        this.add(new CustomLine("customDivider", 274));
        this.add(new FileTabOptionButtonFoldersOption("foldersOption","Change folders and search options", false));
        this.add(new FileTabOptionButtonHelp("help","Help", true));
        this.add(new FileTabOptionButtonClose("close","Close", false));
    }
}
