package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.fileTabExtendedOptions;

import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.JExplorerWindowsTabTabPane;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.TabSelected;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptionButtonNonExpandable;
import jExplorerView.utility.ExternalBrowserOpener;

public class FileTabOptionsExtendedAboutButton extends FileTabOptionButtonNonExpandable {

    private String urlToAbout;

    public FileTabOptionsExtendedAboutButton(String idName, String buttonText, boolean isExpandable) {
        super(idName, buttonText, isExpandable);
        setUrlToAbout("https://google.com");
    }

    @Override
    protected void createOnClickAction() {
        this.setOnMouseClicked(event -> {
            this.getScene().getWindow().hide();
            JExplorerWindowsTabTabPane.getInstance().getSelectionModel().select(TabSelected.NO_SELECT.getValue());
            ExternalBrowserOpener externalBrowserOpener = new ExternalBrowserOpener(getUrlToAbout());
            externalBrowserOpener.openBrowserToUrl();
        });
    }

    public String getUrlToAbout() {
        return urlToAbout;
    }

    public void setUrlToAbout(String urlToAbout) {
        this.urlToAbout = urlToAbout;
    }
}
