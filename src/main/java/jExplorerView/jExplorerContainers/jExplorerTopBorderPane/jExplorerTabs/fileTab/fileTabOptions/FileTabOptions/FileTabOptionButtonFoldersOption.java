package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptions;

import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.JExplorerWindowsTabTabPane;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.TabSelected;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptionButtonNonExpandable;

public class FileTabOptionButtonFoldersOption extends FileTabOptionButtonNonExpandable {
    public FileTabOptionButtonFoldersOption(String idName, String buttonText, boolean isExpandable) {
        super(idName, buttonText, isExpandable);
    }

    @Override
    protected void createOnClickAction() {
        this.setOnMouseClicked(event -> {
            System.out.println("Do file options stuff");
            JExplorerWindowsTabTabPane.getInstance().getSelectionModel().select(TabSelected.NO_SELECT.getValue());
        });
    }
}
