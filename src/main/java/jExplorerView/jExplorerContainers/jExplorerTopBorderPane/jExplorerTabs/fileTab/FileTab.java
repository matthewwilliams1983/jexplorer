package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab;

import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.input.MouseEvent;
import jExplorerView.jExplorerContainers.MainStage;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.CustomTab;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.JExplorerWindowsTabTabPane;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.TabSelected;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptions.FileTabOptionsStage;


public class FileTab extends CustomTab {

    private FileTabOptionsStage fileOptionsStage;
    private SingleSelectionModel<Tab> selectionModel;


    public FileTab(String idName, String tabText){

        this.setId(idName);
        this.setText(tabText);
        selectionModel = JExplorerWindowsTabTabPane.getInstance().getSelectionModel();

        //Create fileOptions stage and scene
        fileOptionsStage = new FileTabOptionsStage();

        //Manage logic for showing the fileOptions stage
        showFileTabOptionsStage();

        //Manage logic for hiding fileTabOptions stage
        hideFileTabOptionStage();
    }

    private void showFileTabOptionsStage() {
        this.setOnSelectionChanged(event -> {
            if(this.isSelected() && !fileOptionsStage.isShowing()){
                fileOptionsStage.setX(MainStage.getPrimaryStage().getX());
                fileOptionsStage.setY(MainStage.getPrimaryStage().getY() + 60);
                fileOptionsStage.show();
                fileOptionsStage.requestFocus();
            }
            else{
                fileOptionsStage.close();
            }
        });
    }

    private void hideFileTabOptionStage(){
        MainStage.getPrimaryStage().addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
            if (!fileOptionsStage.isFocused() && fileOptionsStage.isShowing()) {
                fileOptionsStage.hide();
                selectionModel.select(TabSelected.NO_SELECT.getValue());//Select blank tab
                selectionModel.clearSelection();
            }
        });
    }

    @Override
    public String getTabText() {
        return this.getText();
    }

    @Override
    public String getIdName() {
        return this.getId();
    }

    @Override
    public boolean wasSelected() {
        return false;
    }

    @Override
    public void setWasSelected(boolean wasSelected) {

    }
}
