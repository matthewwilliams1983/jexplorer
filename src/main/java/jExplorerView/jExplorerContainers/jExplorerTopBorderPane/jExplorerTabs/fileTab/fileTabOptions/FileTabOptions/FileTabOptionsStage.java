package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptions;

import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import jExplorerView.jExplorerContainers.MainStage;
import jExplorerView.utility.ChildrenStageHandler;

public class FileTabOptionsStage extends Stage {

    private double sceneWidth = 550;
    private double sceneHeight = 236;

    public FileTabOptionsStage(){
        ChildrenStageHandler.getInstance().addStageToOpenStageList(this);
        this.initStyle(StageStyle.UNDECORATED);
        this.initModality(Modality.NONE);
        this.initOwner(MainStage.getPrimaryStage());
        FileTabVBox fileTabVBox = new FileTabVBox();
        Scene fileTabScene = new Scene(fileTabVBox, getSceneWidth(), getSceneHeight());
        this.setScene(fileTabScene);
        this.hide();
    }

    public FileTabOptionsStage(double sceneWidth, double sceneHeight){
        this.setSceneWidth(sceneWidth);
        this.setSceneHeight(sceneHeight);
        this.initStyle(StageStyle.UNDECORATED);
        this.initModality(Modality.NONE);
        this.initOwner(MainStage.getPrimaryStage());
        FileTabVBox fileTabVBox = new FileTabVBox();
        Scene fileTabScene = new Scene(fileTabVBox, getSceneWidth(), getSceneHeight());
        this.setScene(fileTabScene);
        this.hide();
    }


    public double getSceneWidth() {
        return sceneWidth;
    }

    public void setSceneWidth(double sceneWidth) {
        this.sceneWidth = sceneWidth;
    }

    public double getSceneHeight() {
        return sceneHeight;
    }

    public void setSceneHeight(double sceneHeight) {
        this.sceneHeight = sceneHeight;
    }
}
