package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.fileTabExtendedOptions;

import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.JExplorerWindowsTabTabPane;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.TabSelected;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.fileTabOptions.FileTabOptionButtonNonExpandable;
import jExplorerView.utility.ExternalBrowserOpener;

public class FileTabOptionsExtendedHelpButton extends FileTabOptionButtonNonExpandable {

    private String urlToHelp;

    public FileTabOptionsExtendedHelpButton(String idName, String buttonText, boolean isExpandable) {
        super(idName, buttonText, isExpandable);
        setUrlToHelp("https://google.com");
    }

    @Override
    protected void createOnClickAction() {
        this.setOnMouseClicked(event -> {
            this.getScene().getWindow().hide();
            JExplorerWindowsTabTabPane.getInstance().getSelectionModel().select(TabSelected.NO_SELECT.getValue());
            ExternalBrowserOpener externalBrowserOpener = new ExternalBrowserOpener(getUrlToHelp());
            externalBrowserOpener.openBrowserToUrl();
        });
    }

    public String getUrlToHelp() {
        return urlToHelp;
    }

    public void setUrlToHelp(String urlToHelp) {
        this.urlToHelp = urlToHelp;
    }
}
