package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs;

import javafx.scene.control.Tab;

public abstract class CustomTab extends Tab{

    public abstract String getTabText();
    public abstract String getIdName();
    public abstract boolean wasSelected();
    public abstract void setWasSelected(boolean wasSelected);

}
