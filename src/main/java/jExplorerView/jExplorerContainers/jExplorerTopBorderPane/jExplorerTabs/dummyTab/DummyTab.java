package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.dummyTab;

import javafx.scene.control.TabPane;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.CustomTab;

public class DummyTab extends CustomTab {

    private boolean wasSelected;

    public DummyTab(String idName, String tabText, TabPane tabPane){
        this.setId(idName);
        this.setText(tabText);
        this.setContent(new JExplorerDummyTabContentHBox());

        this.setOnSelectionChanged(event -> {

            if(this.isSelected()) {
                CustomTab previousTab = (CustomTab)tabPane.getTabs().get(2);
                if(previousTab.wasSelected()){
                    previousTab.setWasSelected(false);
                }
            }
            wasSelected=true;
        });
    }

    @Override
    public String getTabText() {
        return null;
    }

    @Override
    public String getIdName() {
        return null;
    }

    @Override
    public boolean wasSelected() {
        return false;
    }

    @Override
    public void setWasSelected(boolean wasSelected) {

    }
}
