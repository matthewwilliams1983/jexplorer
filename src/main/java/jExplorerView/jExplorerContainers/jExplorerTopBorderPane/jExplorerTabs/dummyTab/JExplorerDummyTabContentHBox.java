package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.dummyTab;

import javafx.scene.layout.HBox;
import javafx.stage.Screen;

public class JExplorerDummyTabContentHBox extends HBox {
    public JExplorerDummyTabContentHBox(){
        super.setWidth(Screen.getPrimary().getVisualBounds().getWidth());
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerDummyTabContentHBox");
    }
}
