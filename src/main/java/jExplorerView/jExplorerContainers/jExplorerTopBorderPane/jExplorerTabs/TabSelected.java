package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs;

public enum TabSelected{
    FILE(0), COMPUTER(1), VIEW(2), THEME(3), NO_SELECT(4);

    private int value;

    TabSelected(int value){
        this.value = value;
    }

    public int getValue(){
        return value;
    }
}
