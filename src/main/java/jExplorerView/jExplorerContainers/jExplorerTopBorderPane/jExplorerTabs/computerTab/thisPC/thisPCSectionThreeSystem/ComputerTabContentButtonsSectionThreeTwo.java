package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionThreeSystem;

import javafx.scene.control.Button;

import java.util.ArrayList;

public class ComputerTabContentButtonsSectionThreeTwo extends ArrayList<Button> {
    public ComputerTabContentButtonsSectionThreeTwo() {
        this.add(new ComputerTabSectionThreeUCPButton());
        this.add(new ComputerTabSectionThreeSystemPropertiesButton());
        this.add(new ComputerTabSectionThreeManageButton());
    }
}
