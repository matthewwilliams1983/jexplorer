package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionOneLocation;

import javafx.scene.control.Button;

import java.util.ArrayList;

public class ComputerTabContentButtonsSectionOne extends ArrayList<Button> {
    public ComputerTabContentButtonsSectionOne(){
        this.add(new ComputerTabSectionOnePropertiesButton());
        this.add(new ComputerTabSectionOneOpenButton());
        this.add(new ComputerTabSectionOneRenameButton());
    }
}
