package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionOneLocation;

import javafx.scene.layout.HBox;

public class ComputerTabContentSectionHBoxButtonsSectionOne extends HBox {
    public ComputerTabContentSectionHBoxButtonsSectionOne(){
        this.setId("computerTabContentSectionHBoxButtonsSectionOne");
        this.getStylesheets().add("css/defaultTheme.css");
        this.getChildren().addAll(new ComputerTabContentButtonsSectionOne());
    }
}
