package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab;

import javafx.scene.Cursor;
import javafx.scene.control.*;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.CustomTab;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.JExplorerWindowsTabTabPane;

public class ComputerTab extends CustomTab {

    private boolean wasSelected;
    private SingleSelectionModel<Tab> selectionModel;

    public ComputerTab(String idName, String tabText, TabPane tabPane){
        this.setId(idName);
        this.setText(tabText);
        selectionModel = JExplorerWindowsTabTabPane.getInstance().getSelectionModel();
        this.setContent(new ComputerTabContentHolder());

        this.setOnSelectionChanged(event -> {
            tabPane.setCursor(Cursor.DEFAULT);
            wasSelected=true;
        });
    }

    @Override
    public String getTabText() {
        return this.getText();
    }

    @Override
    public String getIdName() {
        return this.getId();
    }

    @Override
    public boolean wasSelected() {
        return wasSelected;
    }

    @Override
    public void setWasSelected(boolean wasSelected) {
        this.wasSelected = wasSelected;
    }

}
