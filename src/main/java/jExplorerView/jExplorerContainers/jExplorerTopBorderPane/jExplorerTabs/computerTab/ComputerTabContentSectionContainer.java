package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab;

import javafx.scene.Node;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class ComputerTabContentSectionContainer extends VBox {
    public ComputerTabContentSectionContainer(ArrayList<Node> sectionContent){
        this.setId("computerTabContentSectionContainer");
        this.getStylesheets().add("css/defaultTheme.css");
        this.getChildren().addAll(sectionContent);
    }
}
