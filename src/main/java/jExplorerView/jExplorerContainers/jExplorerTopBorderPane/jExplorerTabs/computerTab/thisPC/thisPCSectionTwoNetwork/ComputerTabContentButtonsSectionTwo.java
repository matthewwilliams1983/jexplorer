package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionTwoNetwork;

import javafx.scene.control.Button;

import java.util.ArrayList;

public class ComputerTabContentButtonsSectionTwo extends ArrayList<Button> {
    public ComputerTabContentButtonsSectionTwo(){
        this.add(new ComputerTabSectionTwoAccessMediaButton());
        this.add(new ComputerTabSectionTwoMapNetworkDriveButton());
        this.add(new ComputerTabSectionTwoAddANetworkLocationButton());
    }
}
