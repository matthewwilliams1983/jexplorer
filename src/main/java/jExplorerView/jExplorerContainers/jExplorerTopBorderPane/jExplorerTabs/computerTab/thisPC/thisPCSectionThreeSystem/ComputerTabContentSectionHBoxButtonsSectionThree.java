package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionThreeSystem;

import javafx.scene.layout.HBox;

public class ComputerTabContentSectionHBoxButtonsSectionThree extends HBox {
    public ComputerTabContentSectionHBoxButtonsSectionThree(){
        this.setId("computerTabContentSectionHBoxButtonsSectionThree");
        this.getStylesheets().add("css/defaultTheme.css");
        this.getChildren().addAll(new ComputerTabContentButtonsSectionThreeOne());
        this.getChildren().add(new ComputerTabContentVBoxButtonsSectionThree());
    }
}
