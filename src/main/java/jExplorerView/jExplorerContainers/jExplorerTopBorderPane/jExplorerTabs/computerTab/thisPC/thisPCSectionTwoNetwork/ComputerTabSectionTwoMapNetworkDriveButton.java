package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionTwoNetwork;

import javafx.scene.control.Button;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class ComputerTabSectionTwoMapNetworkDriveButton extends Button {

    private static final Logger LOGGER = LogManager.getLogger("JExplorer");

    public ComputerTabSectionTwoMapNetworkDriveButton(){
        this.setId("computerTabSectionTwoMapNetworkDriveMediaButton");
        this.getStylesheets().add("css/defaultTheme.css");
        this.setOnMouseClicked(event -> {
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec("cmd /c start ms-settings:about");
            } catch (IOException e) {
                LOGGER.error(this.getClass() + ": " + e.toString());
            }
        });
    }
}
