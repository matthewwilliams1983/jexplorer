package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionThreeSystem;

import javafx.scene.layout.VBox;

public class ComputerTabContentVBoxButtonsSectionThree extends VBox {
    public ComputerTabContentVBoxButtonsSectionThree(){
        this.getChildren().addAll(new ComputerTabContentButtonsSectionThreeTwo());
    }
}
