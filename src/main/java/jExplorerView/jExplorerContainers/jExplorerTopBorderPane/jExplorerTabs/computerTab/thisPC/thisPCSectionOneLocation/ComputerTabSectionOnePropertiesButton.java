package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionOneLocation;

import javafx.scene.control.Button;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class ComputerTabSectionOnePropertiesButton extends Button {

    private static final Logger LOGGER = LogManager.getLogger("JExplorer");

    public ComputerTabSectionOnePropertiesButton(){
        this.setId("computerTabSectionOnePropertiesButton");
        this.getStylesheets().add("css/defaultTheme.css");
        this.setOnMouseClicked(event -> {
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec("cmd /c control system");
            } catch (IOException e) {
                LOGGER.error(this.getClass() + ": " + e.toString());
            }
        });
    }
}
