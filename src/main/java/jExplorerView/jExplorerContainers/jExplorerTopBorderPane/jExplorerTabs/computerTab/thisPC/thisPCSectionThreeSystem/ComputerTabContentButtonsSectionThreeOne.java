package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionThreeSystem;

import javafx.scene.control.Button;

import java.util.ArrayList;

public class ComputerTabContentButtonsSectionThreeOne extends ArrayList<Button> {
    public ComputerTabContentButtonsSectionThreeOne() {
        this.add(new ComputerTabSectionThreeOpenSettingsButton());
    }
}
