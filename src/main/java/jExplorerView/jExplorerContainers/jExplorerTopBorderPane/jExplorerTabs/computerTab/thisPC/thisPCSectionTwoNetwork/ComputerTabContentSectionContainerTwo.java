package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionTwoNetwork;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.TabContentSectionHBoxLabel;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.tabUtility.TabContentSpacer;

import java.util.ArrayList;

public class ComputerTabContentSectionContainerTwo extends ArrayList<Node> {
    public ComputerTabContentSectionContainerTwo(){
        this.add(new ComputerTabContentSectionHBoxButtonsSectionTwo());
        this.add(new TabContentSpacer(Color.TRANSPARENT, 0, 2));
        this.add(new TabContentSectionHBoxLabel("Network"));
    }
}
