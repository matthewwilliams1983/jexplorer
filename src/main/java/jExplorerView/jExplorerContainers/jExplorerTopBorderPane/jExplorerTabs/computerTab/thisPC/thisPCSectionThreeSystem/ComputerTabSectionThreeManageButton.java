package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionThreeSystem;

import javafx.scene.control.Button;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class ComputerTabSectionThreeManageButton extends Button {

    private static final Logger LOGGER = LogManager.getLogger("JExplorer");

    public ComputerTabSectionThreeManageButton(){
        this.setId("computerTabSectionThreeManageButton");
        this.getStylesheets().add("css/defaultTheme.css");
        this.setOnMouseClicked(event -> {
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec("cmd /c start compmgmt.msc");
            } catch (IOException e) {
                LOGGER.error(this.getClass() + ": " + e.toString());
            }
        });
    }
}
