package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionOneLocation;

import javafx.scene.control.Button;

public class ComputerTabSectionOneOpenButton extends Button {
    public ComputerTabSectionOneOpenButton(){
        this.setId("computerTabSectionOneOpenButton");
        this.getStylesheets().add("css/defaultTheme.css");
        this.setOnMouseClicked(event -> {

        });
    }
}
