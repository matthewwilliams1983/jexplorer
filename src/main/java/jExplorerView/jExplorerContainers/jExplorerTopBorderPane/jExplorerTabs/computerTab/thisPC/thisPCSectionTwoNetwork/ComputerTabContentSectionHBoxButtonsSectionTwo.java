package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionTwoNetwork;

import javafx.scene.layout.HBox;

public class ComputerTabContentSectionHBoxButtonsSectionTwo extends HBox {
    public ComputerTabContentSectionHBoxButtonsSectionTwo(){
        this.setId("computerTabContentSectionHBoxButtonsSectionTwo");
        this.getStylesheets().add("css/defaultTheme.css");
        this.getChildren().addAll(new ComputerTabContentButtonsSectionTwo());
    }
}
