package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab;

import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionOneLocation.ComputerTabContentSectionContainerOne;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionThreeSystem.ComputerTabContentSectionContainerThree;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionTwoNetwork.ComputerTabContentSectionContainerTwo;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.tabUtility.CustomSeparator;

public class ComputerTabContentHolder extends HBox {
    public ComputerTabContentHolder(){
        this.setWidth(Screen.getPrimary().getVisualBounds().getWidth());
        this.setId("computerTabContentHolder");
        this.getStylesheets().add("css/defaultTheme.css");
        this.getChildren().add(new ComputerTabContentSectionContainer(new ComputerTabContentSectionContainerOne()));
        this.getChildren().add(new CustomSeparator());
        this.getChildren().add(new ComputerTabContentSectionContainer(new ComputerTabContentSectionContainerTwo()));
        this.getChildren().add(new CustomSeparator());
        this.getChildren().add(new ComputerTabContentSectionContainer(new ComputerTabContentSectionContainerThree()));
        this.getChildren().add(new CustomSeparator());
    }
}
