package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionOneLocation;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.TabContentSectionHBoxLabel;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.tabUtility.TabContentSpacer;

import java.util.ArrayList;

public class ComputerTabContentSectionContainerOne extends ArrayList<Node> {
    public ComputerTabContentSectionContainerOne(){
        this.add(new ComputerTabContentSectionHBoxButtonsSectionOne());
        this.add(new TabContentSpacer(Color.TRANSPARENT, 0, 15));
        this.add(new TabContentSectionHBoxLabel("Location"));
    }
}
