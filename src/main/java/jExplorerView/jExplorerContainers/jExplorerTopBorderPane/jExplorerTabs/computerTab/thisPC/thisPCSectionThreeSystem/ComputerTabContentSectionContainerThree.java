package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.thisPC.thisPCSectionThreeSystem;

import javafx.scene.Node;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.TabContentSectionHBoxLabel;

import java.util.ArrayList;

public class ComputerTabContentSectionContainerThree extends ArrayList<Node> {
    public ComputerTabContentSectionContainerThree(){
        this.add(new ComputerTabContentSectionHBoxButtonsSectionThree());
        this.add(new TabContentSectionHBoxLabel("System"));
    }
}
