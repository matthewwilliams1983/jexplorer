package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.viewTab;

import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.AbstractJExplorerTabContentHBox;

public class JExplorerViewTabContentHBox extends AbstractJExplorerTabContentHBox {
    public JExplorerViewTabContentHBox(){
        super("jExplorerViewTabContentHBox");
    }
}