package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.viewTab;

import javafx.scene.control.TabPane;

import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.CustomTab;

public class ViewTab extends CustomTab {

    private boolean wasSelected;

    public ViewTab(String idName, String tabText, TabPane tabPane){
        this.setId(idName);
        this.setText(tabText);
        super.setContent(new JExplorerViewTabContentHBox());

        this.setOnSelectionChanged(event -> {
            if(this.isSelected()) {
                CustomTab previousTab = (CustomTab)tabPane.getTabs().get(1);
                if(previousTab.wasSelected()){
                    previousTab.setWasSelected(false);
                }
            }
            wasSelected=true;
        });
    }

    @Override
    public String getTabText() {
        return this.getText();
    }

    @Override
    public String getIdName() {
        return this.getId();
    }

    @Override
    public boolean wasSelected() {
        return wasSelected;
    }

    @Override
    public void setWasSelected(boolean wasSelected) {
        this.wasSelected = wasSelected;
    }
}
