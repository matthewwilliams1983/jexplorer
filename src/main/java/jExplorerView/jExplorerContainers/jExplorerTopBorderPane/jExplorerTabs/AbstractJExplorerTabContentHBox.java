package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs;

import javafx.scene.layout.HBox;
import javafx.stage.Screen;

public abstract class AbstractJExplorerTabContentHBox extends HBox {
    public AbstractJExplorerTabContentHBox(String idName){
        super.setWidth(Screen.getPrimary().getVisualBounds().getWidth());
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId(idName);
    }
}
