package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs;

import javafx.scene.control.TabPane;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.computerTab.ComputerTab;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.dummyTab.DummyTab;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.fileTab.FileTab;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.themeTab.ThemeTab;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.viewTab.ViewTab;

public class TabFactory {

    public static CustomTab getCustomTab(String idName, String tabText, TabPane tabPane){
        if(idName.equalsIgnoreCase("fileTab")){
            return new FileTab(idName, tabText);
        }else if(idName.equalsIgnoreCase("computerTab")){
            return new ComputerTab(idName, tabText, tabPane);
        }else if(idName.equalsIgnoreCase("viewTab")) {
            return new ViewTab(idName, tabText, tabPane);
        }else if(idName.equalsIgnoreCase("dummyTab")){
            return new DummyTab(idName, tabText, tabPane);
        }else{
            return new ThemeTab(idName, tabText, tabPane);
        }
    }
}
