package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

import java.util.ArrayList;
import java.util.List;

public class JExplorerWindowsTabs {

    private List<Tab> tabList;

    public JExplorerWindowsTabs(TabPane tabPane){

        Tab fileTab = TabFactory.getCustomTab("fileTab", "File", tabPane);
        Tab computerTab = TabFactory.getCustomTab("computerTab", "Computer", tabPane);
        Tab viewTab = TabFactory.getCustomTab("viewTab", "View", tabPane);
        Tab themeTab = TabFactory.getCustomTab("themeTab", "Theme", tabPane);
        Tab dummyTab = TabFactory.getCustomTab("dummyTab", "", tabPane);
        setTabList(new ArrayList<>());
        getTabList().add(fileTab);
        getTabList().add(computerTab);
        getTabList().add(viewTab);
        getTabList().add(themeTab);
        getTabList().add(dummyTab);
        JExplorerWindowsTabTabPane.getInstance().getSelectionModel().select(TabSelected.NO_SELECT.getValue());
    }


    public List<Tab> getTabList() {
        return tabList;
    }

    public void setTabList(List<Tab> tabList) {
        this.tabList = tabList;
    }
}
