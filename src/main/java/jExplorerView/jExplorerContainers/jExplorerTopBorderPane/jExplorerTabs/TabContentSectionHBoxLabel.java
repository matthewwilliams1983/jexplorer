package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs;

import javafx.scene.layout.HBox;

public class TabContentSectionHBoxLabel extends HBox {

    public TabContentSectionHBoxLabel(String labelText){
        this.getChildren().add(new TabContentSectionLabel(labelText));
    }
}
