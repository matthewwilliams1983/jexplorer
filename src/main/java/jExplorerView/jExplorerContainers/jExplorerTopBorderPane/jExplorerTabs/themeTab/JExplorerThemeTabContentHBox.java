package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.themeTab;

import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.AbstractJExplorerTabContentHBox;

public class JExplorerThemeTabContentHBox extends AbstractJExplorerTabContentHBox {
    public JExplorerThemeTabContentHBox(){
        super("jExplorerThemeTabContentHBox");
    }
}
