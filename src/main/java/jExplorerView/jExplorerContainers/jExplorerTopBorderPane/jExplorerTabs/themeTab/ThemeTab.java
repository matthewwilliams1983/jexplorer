package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.themeTab;

import javafx.scene.control.TabPane;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.CustomTab;

public class ThemeTab extends CustomTab {

    private boolean wasSelected;

    public ThemeTab(String idName, String tabText, TabPane tabPane){
        super.setId(idName);
        super.setText(tabText);
        super.setContent(new JExplorerThemeTabContentHBox());

        this.setOnSelectionChanged(event -> {

            if(this.isSelected()) {
                CustomTab previousTab = (CustomTab)tabPane.getTabs().get(2);
                if(previousTab.wasSelected()){
                    previousTab.setWasSelected(false);
                }
            }
            wasSelected=true;
        });
    }

    @Override
    public String getTabText() {
        return this.getText();
    }

    @Override
    public String getIdName() {
        return this.getId();
    }

    @Override
    public boolean wasSelected() {
        return wasSelected;
    }

    @Override
    public void setWasSelected(boolean wasSelected) {
        this.wasSelected = wasSelected;
    }
}
