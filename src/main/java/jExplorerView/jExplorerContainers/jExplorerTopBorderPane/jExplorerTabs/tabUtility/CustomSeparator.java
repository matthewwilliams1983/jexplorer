package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.tabUtility;

import javafx.scene.control.Separator;

public class CustomSeparator extends Separator {
    public CustomSeparator(){
        this.setId("customSeparator");
        this.getStylesheets().add("css/defaultTheme.css");
    }
}