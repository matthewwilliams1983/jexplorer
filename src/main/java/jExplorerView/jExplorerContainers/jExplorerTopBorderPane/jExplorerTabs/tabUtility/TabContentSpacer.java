package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.tabUtility;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;


public class TabContentSpacer extends Rectangle {
    public TabContentSpacer(Color color, double width, double height){
        this.setFill(color);
        this.setWidth(width);
        this.setHeight(height);
    }
}
