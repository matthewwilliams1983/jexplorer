package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs;

import javafx.scene.control.Label;

public class TabContentSectionLabel extends Label {
    public TabContentSectionLabel(String labelText){
        this.setId(labelText.toLowerCase() + "TabContentSectionLabel");
        this.getStylesheets().add("css/defaultTheme.css");
        this.setText(labelText);
    }
}
