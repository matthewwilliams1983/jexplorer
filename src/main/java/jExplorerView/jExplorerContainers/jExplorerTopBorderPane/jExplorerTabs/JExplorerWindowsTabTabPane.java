package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs;

import javafx.geometry.Insets;
import javafx.scene.control.TabPane;

public class JExplorerWindowsTabTabPane extends TabPane{
    private static JExplorerWindowsTabTabPane jExplorerWindowsTabTabPane;

    public static JExplorerWindowsTabTabPane getInstance() {
        if(jExplorerWindowsTabTabPane == null){
            jExplorerWindowsTabTabPane = new JExplorerWindowsTabTabPane();
        }
        return jExplorerWindowsTabTabPane;
    }

    private JExplorerWindowsTabTabPane() {
        this.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        this.setPadding(new Insets(-5, -5, 0, -5));
        this.getSelectionModel().clearSelection();
        this.getStylesheets().add("css/defaultTheme.css");
        this.setId("jExplorerWindowsTabTabPane");
    }
}
