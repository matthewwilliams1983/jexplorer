package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs;
import javafx.scene.layout.HBox;

public class JExplorerWindowsFileTabHBox extends HBox{
    public JExplorerWindowsFileTabHBox(){
        super.setId("jExplorerWindowsFileTabHBox");

        JExplorerWindowsTabTabPane tabPane = JExplorerWindowsTabTabPane.getInstance();
        JExplorerWindowsTabs JExplorerWindowsTabs = new JExplorerWindowsTabs(tabPane);
        tabPane.getTabs().addAll(JExplorerWindowsTabs.getTabList());
        super.getChildren().addAll(tabPane);
    }
}
