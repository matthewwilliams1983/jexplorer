package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTitleBar;

import jExplorerControllers.jExplorerTopControllers.JExplorerTitleBarController;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTitleBar.jExplorerTitleBarButtons.JExplorerTitleBarButtons;
import jExplorerView.utility.WindowMaximizer;
import javafx.scene.layout.HBox;
import jExplorerView.utility.JExplorerHorizontalRegionSpacer;

public class JExplorerTitleBarHBox extends HBox{

    public JExplorerTitleBarHBox(){
        super.setId("jExplorerTitleBarHBox");

        JExplorerTitleBarController jExplorerTitleBarController = new JExplorerTitleBarController(this);
        jExplorerTitleBarController.setMouseEvents();
        WindowMaximizer windowMaximizer = jExplorerTitleBarController.getWindowMaximizer();

        super.getChildren().add(new JExplorerHorizontalRegionSpacer());
        super.getChildren().addAll(new JExplorerTitleBarButtons(windowMaximizer));
    }
}
