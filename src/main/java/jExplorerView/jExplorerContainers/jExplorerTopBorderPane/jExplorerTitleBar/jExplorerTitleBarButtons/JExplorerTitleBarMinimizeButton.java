package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTitleBar.jExplorerTitleBarButtons;

public class JExplorerTitleBarMinimizeButton extends JExplorerTitleBarButton {

    public JExplorerTitleBarMinimizeButton(String idName){
        super.setId(idName);
        super.setText("\u2013");
        super.getjExplorerTitleBarButtonController().setMinimizeButtonMouseEvents();
    }
}
