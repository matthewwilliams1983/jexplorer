package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTitleBar.jExplorerTitleBarButtons;

import jExplorerView.utility.WindowMaximizer;

public class JExplorerTitleBarMaximizeButton extends JExplorerTitleBarButton {

    public JExplorerTitleBarMaximizeButton(String idName, WindowMaximizer windowMaximizer){
        super.setId(idName);
        super.getjExplorerTitleBarButtonController().setMaximizeButtonMouseEvents();
        super.getjExplorerTitleBarButtonController().setWindowMaximizer(windowMaximizer);
        super.getjExplorerTitleBarButtonController().setListenerForWindowMaximization();
        super.setText("\u2610");
    }
}
