package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTitleBar.jExplorerTitleBarButtons;

import jExplorerView.utility.WindowMaximizer;

public class JExplorerTitleBarButtonFactory {
    public static JExplorerTitleBarButton getJExplorerTitleBarButton(String idName){
        if (idName.equalsIgnoreCase("jExplorerTitleBarMinimizeButton")){
            return new JExplorerTitleBarMinimizeButton(idName);
        }else{
            return new JExplorerTitleBarCloseButton(idName);
        }
    }
    public static JExplorerTitleBarButton getJExplorerTitleBarButton(String idName, WindowMaximizer windowMaximizer){
        return new JExplorerTitleBarMaximizeButton(idName, windowMaximizer);
    }
}
