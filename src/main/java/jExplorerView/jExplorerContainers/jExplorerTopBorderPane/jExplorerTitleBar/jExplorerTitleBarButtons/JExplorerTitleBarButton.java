package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTitleBar.jExplorerTitleBarButtons;

import jExplorerControllers.jExplorerTopControllers.JExplorerTitleBarButtonController;
import javafx.scene.Cursor;
import javafx.scene.control.Button;

public abstract class JExplorerTitleBarButton extends Button {
    private JExplorerTitleBarButtonController jExplorerTitleBarButtonController;
    public JExplorerTitleBarButton(){
        this.setjExplorerTitleBarButtonController(new JExplorerTitleBarButtonController(this));
        this.setOnMouseEntered(event -> {
            super.setCursor(Cursor.DEFAULT);
        });
    }

    public JExplorerTitleBarButtonController getjExplorerTitleBarButtonController() {
        return jExplorerTitleBarButtonController;
    }

    public void setjExplorerTitleBarButtonController(JExplorerTitleBarButtonController jExplorerTitleBarButtonController) {
        this.jExplorerTitleBarButtonController = jExplorerTitleBarButtonController;
    }
}
