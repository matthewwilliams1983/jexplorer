package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTitleBar.jExplorerTitleBarButtons;

public class JExplorerTitleBarCloseButton extends JExplorerTitleBarButton {

    public JExplorerTitleBarCloseButton(String buttonId){
        super.setText("\u2715");
        super.setId(buttonId);
        super.getjExplorerTitleBarButtonController().setCloseButtonMouseEvents();
    }

}
