package jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTitleBar.jExplorerTitleBarButtons;

import jExplorerView.utility.WindowMaximizer;
import javafx.scene.control.Button;

import java.util.ArrayList;

public class JExplorerTitleBarButtons extends ArrayList<Button> {
    public JExplorerTitleBarButtons(WindowMaximizer windowMaximizer){
        super.add(JExplorerTitleBarButtonFactory.getJExplorerTitleBarButton("jExplorerTitleBarMinimizeButton"));
        super.add(JExplorerTitleBarButtonFactory.getJExplorerTitleBarButton("jExplorerTitleBarMaximizeButton", windowMaximizer));
        super.add(JExplorerTitleBarButtonFactory.getJExplorerTitleBarButton("jExplorerTitleBarCloseButton"));
    }
}
