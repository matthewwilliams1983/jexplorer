package jExplorerView.jExplorerContainers.jExplorerTopBorderPane;

import javafx.scene.layout.*;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerNavigation.JExplorerNavigationHBox;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.JExplorerWindowsFileTabHBox;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTitleBar.JExplorerTitleBarHBox;

/*
 * Purpose - This class is the VBox that holds the top part of the BorderPane.
 *           See drawing below....
 *   _______________
 *   |____This_____|
 *   | |         | |
 *   |L|    C    |R|
 *   | |         | |
 *   | |_________| |
 *   |______B______|
 *
 * @author Matthew Williams
 * @since 2018-08-11
 */

public class JExplorerTopToolBarVBox extends VBox{

    /*
     * Purpose - This constructor creates a new VBox that will hold the minimize, maximize, and close button that gets
     *           placed into the top section of the BorderPane. Also, this section contains the jExplorerTabs that define the
     *           options for the JExplorer as well as the jExplorerNavigation bar.
     */
    public JExplorerTopToolBarVBox() {
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerTopToolBarVBox");

        super.getChildren().add(new JExplorerTitleBarHBox());
        super.getChildren().add(new JExplorerWindowsFileTabHBox());
        super.getChildren().add(new JExplorerNavigationHBox());
    }//EoM JExplorerTopToolBarVBox Constructor--------------------------------------------------------------------------
}


