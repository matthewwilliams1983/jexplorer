package jExplorerView.jExplorerContainers.jExplorerLeftBorderPane;

import jExplorerControllers.JExplorerStartViewController;
import javafx.scene.Cursor;
import javafx.scene.control.TreeView;
import jExplorerData.RootDirectory;
import jExplorerData.jExplorerDirectoryUtility.JExplorerTreeItemEntry;

/*
 * Purpose - This class is the TreeView of the JExplorer.  It sets the view with the contents of the users root
 *           directory. See drawing below...
 *
 *   _______________
 *   |______T______|
 *   |T|         | |
 *   |H|    C    |R|
 *   |I|         | |
 *   |S|_________| |
 *   |______B______|
 *
 * @author Matthew Williams
 * @since 2018-08-11
 */

public class JExplorerLeftSideExplorerTreeView extends TreeView<JExplorerTreeItemEntry> {

    /*
     * Purpose - Constructs a new JExplorerLeftSideExplorerTreeView.
     * @param customSideExplorerTreeView - The TreeView whose selection will determine how to populate this
     *                                     TableView's jExplorerData.
     */
    public JExplorerLeftSideExplorerTreeView() {
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerLeftSideExplorerTreeView");
        super.setRoot(new RootDirectory());
        super.setOnMouseEntered(mouseEnteredEvent ->{
            super.setCursor(Cursor.DEFAULT);
        });
        JExplorerStartViewController.getInstance().setjExplorerLeftSideExplorerTreeView(this);
        JExplorerStartViewController.getInstance().selectTreeItemOnLoad();
    }//EoM JExplorerLeftSideExplorerTreeView Constructor-----------------------------------------------------------------------
}//EoC JExplorerLeftSideExplorerTreeView---------------------------------------------------------------------------------------
