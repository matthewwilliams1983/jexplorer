package jExplorerView.jExplorerContainers.jExplorerLeftBorderPane;

import javafx.scene.Cursor;
import javafx.scene.layout.Region;
import jExplorerView.jExplorerContainers.MainStage;

public class JExplorerLeftExplorerTreeViewRegionDivider extends Region {
    public JExplorerLeftExplorerTreeViewRegionDivider(JExplorerLeftSideExplorerTreeView jExplorerLeftSideExplorerTreeView){
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerLeftExplorerTreeViewRegionDivider");
        super.setOnMouseEntered(mouseEventEntered -> {
            super.setCursor(Cursor.H_RESIZE);
        });
        super.setOnMouseDragged(mouseEventDrag -> {
            jExplorerLeftSideExplorerTreeView.setPrefWidth(mouseEventDrag.getSceneX());
            jExplorerLeftSideExplorerTreeView.setMaxWidth(MainStage.getPrimaryStage().getWidth() - 200);
        });
    }
}
