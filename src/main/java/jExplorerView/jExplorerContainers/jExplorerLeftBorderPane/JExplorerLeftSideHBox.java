package jExplorerView.jExplorerContainers.jExplorerLeftBorderPane;

import javafx.scene.layout.HBox;

public class JExplorerLeftSideHBox extends HBox {
    public JExplorerLeftSideHBox(JExplorerLeftSideExplorerTreeView jExplorerLeftSideExplorerTreeView){
        super.getStylesheets().add("css/defaultTheme.css");
        super.setId("jExplorerLeftSideHBox");
        super.getChildren().add(jExplorerLeftSideExplorerTreeView);
        super.getChildren().add(new JExplorerLeftExplorerTreeViewRegionDivider(jExplorerLeftSideExplorerTreeView));
    }
}
