package jExplorerView.utility;

import javafx.stage.Stage;

import java.util.ArrayList;

public class ChildrenStageHandler {

    private static ChildrenStageHandler childrenStageHandler;
    private ArrayList<Stage> stageArrayList;


    public static ChildrenStageHandler getInstance(){
        if (childrenStageHandler == null){
            childrenStageHandler = new ChildrenStageHandler();
        }
        return childrenStageHandler;
    }

    private ChildrenStageHandler(){
        stageArrayList = new ArrayList<Stage>();
    }

    public void addStageToOpenStageList(Stage stage){
        stageArrayList.add(stage);
    }
    public void hideAllStagesInList(){
        for(Stage stage: stageArrayList){
            stage.hide();
        }
    }

}
