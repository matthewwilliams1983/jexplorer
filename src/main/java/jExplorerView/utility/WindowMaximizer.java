package jExplorerView.utility;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Window;
import jExplorerView.jExplorerContainers.MainStage;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class WindowMaximizer {

    private PropertyChangeSupport propertyChangeSupport;
    private Window stageToMaximize;
    private double previousX;
    private double previousY;
    private double previousWidth;
    private double previousHeight;
    private boolean isMaximized;

    public WindowMaximizer(){
        this.stageToMaximize = MainStage.getPrimaryStage();
        propertyChangeSupport = new PropertyChangeSupport(this);
    }

    public void maximizeWindow(){
        setPreviousX(stageToMaximize.getX());
        setPreviousY(stageToMaximize.getY());
        setPreviousWidth(stageToMaximize.getWidth());
        setPreviousHeight(stageToMaximize.getHeight());

        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        stageToMaximize.sizeToScene();
        stageToMaximize.setX(bounds.getMinX());
        stageToMaximize.setY(bounds.getMinY());
        stageToMaximize.setWidth(bounds.getWidth());
        stageToMaximize.setHeight(bounds.getHeight());
        this.setMaximized(true);
    }
    public void undoWindowMaximize(){
        stageToMaximize.setX(this.getPreviousX());
        stageToMaximize.setY(this.getPreviousY());
        stageToMaximize.setWidth(this.getPreviousWidth());
        stageToMaximize.setHeight(this.getPreviousHeight());
        this.setMaximized(false);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public double getPreviousX() {
        return previousX;
    }

    public void setPreviousX(double previousX) {
        this.previousX = previousX;
    }

    public double getPreviousY() {
        return previousY;
    }

    public void setPreviousY(double previousY) {
        this.previousY = previousY;
    }


    public double getPreviousHeight() {
        return previousHeight;
    }

    public void setPreviousHeight(double previousHeight) {
        this.previousHeight = previousHeight;
    }

    public double getPreviousWidth() {
        return previousWidth;
    }

    public void setPreviousWidth(double previousWidth) {
        this.previousWidth = previousWidth;
    }

    public boolean isMaximized() {
        return isMaximized;
    }

    public void setMaximized(boolean maximized) {
        isMaximized = maximized;
        propertyChangeSupport.firePropertyChange("IsMaximized", !isMaximized, isMaximized);
    }
}
