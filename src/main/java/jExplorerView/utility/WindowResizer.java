package jExplorerView.utility;

import javafx.application.Platform;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.JExplorerWindowsTabTabPane;
import jExplorerView.jExplorerContainers.MainStage;
import jExplorerView.jExplorerContainers.jExplorerTopBorderPane.jExplorerTabs.TabSelected;

public class WindowResizer {

    private static boolean lock;
    private boolean hLResize;
    private boolean hRResize;
    private double minWidth = 100;
    private double minHeight = 100;
    private int resizeThreshold = 3;
    private double height;
    private double width;

    public WindowResizer(Pane pane){
        setMinWidth(this.minWidth);
        setMinHeight(this.minHeight);
        setResizeThreshold(this.resizeThreshold);
        setResizeToWindow(pane);
    }

    public WindowResizer(Pane pane, double minWidth, double minHeight){
        setMinWidth(minWidth);
        setMinHeight(minHeight);
        setResizeThreshold(this.resizeThreshold);
        setResizeToWindow(pane);
    }

    public WindowResizer(Pane pane, double minWidth, double minHeight, int resizeThreshold){
        setMinWidth(minWidth);
        setMinHeight(minHeight);
        setResizeThreshold(resizeThreshold);
        setResizeToWindow(pane);
    }

    private void setResizeToWindow(Pane pane){

        pane.setOnMouseMoved(event -> {
            Platform.runLater(() -> {
                if((event.getSceneX() >= MainStage.getPrimaryStage().getWidth() - resizeThreshold) && event.getSceneY() <= resizeThreshold){
                    hRResize = false;
                    lock = true;
                    pane.setCursor(Cursor.NE_RESIZE);
                } else if (event.getSceneX() <= resizeThreshold && event.getSceneY() <= resizeThreshold) {
                    hLResize = false;
                    lock=true;
                    pane.setCursor(Cursor.NW_RESIZE);
                } else if (event.getSceneX() <= resizeThreshold && event.getSceneY() >= MainStage.getPrimaryStage().getHeight() - resizeThreshold) {
                    lock=true;
                    pane.setCursor(Cursor.SW_RESIZE);
                } else if (event.getSceneX() >= MainStage.getPrimaryStage().getWidth() - resizeThreshold && event.getSceneY() >= MainStage.getPrimaryStage().getHeight() - resizeThreshold) {
                    lock=true;
                    pane.setCursor(Cursor.SE_RESIZE);
                } else if (event.getSceneX() >= MainStage.getPrimaryStage().getWidth() - resizeThreshold) {
                    hRResize = true;
                    lock=true;
                    pane.setCursor(Cursor.H_RESIZE);
                }else if (event.getSceneX() <= resizeThreshold) {
                    hLResize = true;
                    lock=true;
                    pane.setCursor(Cursor.H_RESIZE);
                } else if (event.getSceneY() <= resizeThreshold) {
                    lock = true;
                    pane.setCursor(Cursor.N_RESIZE);
                } else if (event.getSceneY() >= MainStage.getPrimaryStage().getHeight() - resizeThreshold) {
                    lock = true;
                    pane.setCursor(Cursor.S_RESIZE);
                } else {
                    pane.setCursor(Cursor.DEFAULT);
                    hLResize = false;
                    hRResize = false;
                    lock = false;
                }
            });
        });
        pane.setOnMouseDragged(event ->{
            Platform.runLater(() -> {
                ChildrenStageHandler.getInstance().hideAllStagesInList();
                JExplorerWindowsTabTabPane.getInstance().getSelectionModel().select(TabSelected.NO_SELECT.getValue());
                if(pane.getCursor().equals(Cursor.S_RESIZE)){
                    adjustSouthResize(event);
                }else if(pane.getCursor().equals(Cursor.N_RESIZE)){
                    adjustNorthResize(event);
                }else if(pane.getCursor().equals(Cursor.H_RESIZE) && hLResize){
                    adjustLeftResize(event);
                }else if(pane.getCursor().equals(Cursor.H_RESIZE) && hRResize){
                    adjustRightResize(event);
                }else if(pane.getCursor().equals(Cursor.NE_RESIZE)){
                    adjustNorthResize(event);
                    adjustRightResize(event);
                }else if(pane.getCursor().equals(Cursor.NW_RESIZE)){
                    adjustNorthResize(event);
                    adjustLeftResize(event);
                }else if(pane.getCursor().equals(Cursor.SE_RESIZE)){
                    adjustSouthResize(event);
                    adjustRightResize(event);
                }else if(pane.getCursor().equals(Cursor.SW_RESIZE)){
                    adjustSouthResize(event);
                    adjustLeftResize(event);
                }
            });

        });
    }

    private void adjustNorthResize(MouseEvent event){
        this.height = MainStage.getPrimaryStage().getY() - event.getScreenY() + MainStage.getPrimaryStage().getHeight();
        if(this.minHeight <= height){
            MainStage.getPrimaryStage().setHeight(MainStage.getPrimaryStage().getY() - event.getScreenY() + MainStage.getPrimaryStage().getHeight());
            MainStage.getPrimaryStage().setY(event.getScreenY());
        }
    }
    private void adjustSouthResize(MouseEvent event){
        this.height = event.getScreenY() - MainStage.getPrimaryStage().getY();
        if(this.minHeight <= this.height){
            MainStage.getPrimaryStage().setHeight(event.getScreenY() - MainStage.getPrimaryStage().getY());
        }
    }

    private void adjustRightResize(MouseEvent event){
        double width = event.getScreenX() - MainStage.getPrimaryStage().getX();
        if(this.minWidth <= width){
            MainStage.getPrimaryStage().setWidth(width);
        }
    }

    private void adjustLeftResize(MouseEvent event){
        this.width = MainStage.getPrimaryStage().getX() - event.getScreenX() + MainStage.getPrimaryStage().getWidth();
        if(this.minWidth <= this.width){
            MainStage.getPrimaryStage().setWidth(MainStage.getPrimaryStage().getX() - event.getScreenX() + MainStage.getPrimaryStage().getWidth());
            MainStage.getPrimaryStage().setX(event.getScreenX());
        }
    }

    public static boolean isLock() {
        return lock;
    }

    public double getMinWidth() {
        return minWidth;
    }

    private void setMinWidth(double minWidth) {
        if(minWidth < 25){
            this.minWidth = 25;
        }else{
            this.minWidth = minWidth;
        }
    }

    public double getMinHeight() {
        return minHeight;
    }

    private void setMinHeight(double minHeight) {
        if(minHeight < 25){
            this.minHeight = 25;
        }else{
            this.minHeight = minHeight;
        }
    }

    public int getResizeThreshold() {
        return resizeThreshold;
    }

    private void setResizeThreshold(int resizeThreshold) {
        this.resizeThreshold = resizeThreshold;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

}
