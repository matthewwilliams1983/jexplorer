package jExplorerView.utility;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class ExternalBrowserOpener {

    private static final Logger LOGGER = LogManager.getLogger("JExplorer");
    private String urlToOpenTo;

    public ExternalBrowserOpener(String url){
        setUrlToOpenTo(url);
    }

    public void openBrowserToUrl(){
        try {
            Desktop.getDesktop().browse(new URL(getUrlToOpenTo()).toURI());
        } catch (IOException e) {
            LOGGER.error(this.getClass() + ": " + e.toString());
        } catch (URISyntaxException e) {
            LOGGER.error(this.getClass() + ": " + e.toString());
        }
    }

    public String getUrlToOpenTo() {
        return urlToOpenTo;
    }

    public void setUrlToOpenTo(String urlToOpenTo) {
        this.urlToOpenTo = urlToOpenTo;
    }
}
