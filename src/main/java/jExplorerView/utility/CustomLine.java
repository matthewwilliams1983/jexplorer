package jExplorerView.utility;

import javafx.scene.shape.Line;

public class CustomLine extends Line {
    public CustomLine(String idName, int endX){
        this.setId(idName);
        this.setEndX(endX);
    }
}
