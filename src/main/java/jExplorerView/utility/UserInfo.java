package jExplorerView.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserInfo {

    private static UserInfo instance;
    private String userName;
    private String userSID;

    private UserInfo(){
        this.setUserName(System.getProperty("user.name"));
        this.setUserSID();
    }

    public static UserInfo getInstance(){
        if(instance == null){
            instance = new UserInfo();
        }
        return instance;
    }

    public String getUserName() {
        return userName;
    }

    private void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSID() {
        return userSID;
    }

    //TODO needs to be more robust
    private void setUserSID() {
        this.userSID = null;
        Runtime runtime = Runtime.getRuntime();
        try {
            Process proc = runtime.exec("cmd /c wmic useraccount where name=\"%username%\" get sid");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String wmicResult = null;
            while((wmicResult = bufferedReader.readLine()) != null){
                if(wmicResult.contains("S-1")){
                    this.userSID = wmicResult.replaceAll("\\s", "");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
