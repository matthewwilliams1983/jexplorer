package jExplorerView.utility;

import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public class JExplorerHorizontalRegionSpacer extends Region {
    public JExplorerHorizontalRegionSpacer(){
        HBox.setHgrow(this, Priority.ALWAYS);
    }
    public JExplorerHorizontalRegionSpacer(double minWidth){
        super.setMinWidth(minWidth);
    }
}
