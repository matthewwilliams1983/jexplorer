# ![JExplorer Folder Icon](https://image.ibb.co/jsYusq/black-Folder50x50.png) JExplorer

## Purpose
JExplorer is a replacement for traditional file explorers.  The concept is to recreate the same functionality of Windows file explorer but offer  
the user the ability to choose from predefined themes as well as create their own in order to customize the look of thier file explorer.  
In the future, JExplorer will allow the user to create or install plugins to extend the functionality of JExplorer.

## Notes
Microsoft recently annouced that in update 1809, Windows file explorer will be supporting a dark theme which was the original intent of this project.

## RoadMap  

* Implementation of all file explorer functionality
* Thematic features
* Cross platform support
* Plugin functionality
* Resource and Manage site

## Current State of the Project
![Current JExplorer](https://i.ibb.co/6wYChbC/JExplorer-Visual-Exp.png)